# 🕹 TakaMan

### TakaMan - O Projeto de PacMan do Time Taka

## 🎮 Como Jogar

Para jogar o **TakaMan**, basta baixar o arquivo *TakaMan.jar* na [Release 3.0 do projeto](https://gitlab.com/timetaka/taka-man/-/releases/3.0), colá-lo na pasta *TakaMan* do projeto, e rodá-la com o seguinte comando:

```C
java -jar TakaMan.jar
```

Outra opção é compilar e rodar a main do projeto pelo Eclipse (sugerimos ao corretor que rode o programa no eclipse pois é a forma como estamos fazendo na matéria em geral).

## ⚙ Funcionalidades

### 🗺 Leitura de Mapas
Dado um arquivo do tipo txt no formato correto, o software é capaz de lê-lo e transformá-lo em um mapa de tamanho 21x27. Cada objeto de jogo é representado por um caractere; o arquivo deve estar no formato de uma matriz 21x27 preenchida por estes caracteres. Atualmente, há apenas um único mapa, baseado no jogo original. Caso deseje alterar o mapa, altere o arquivo na pasta "mapas".  
Abaixo, teremos uma lista do que cada caractere no arquivo TXT representa na hora da criação do mapa:
- ```-``` : Representa uma parede que será criada naquela posição;
- ```.``` : Representa um ponto não comido que será criado naquela posição;
- ```J``` : Representa a posição inicial do PacMan (sugerimos que somente se coloque um J no mapa para evitar problemas);
- ```B``` : Representa a posição inicial de um fantasma Blinky;
- ```P``` : Representa a posição inicial de um fantasma Pinky;
- ```I``` : Representa a posição inicial de um fantasma Inky;
- ```C``` : Representa a posição inicial de um fantasma Clyde;
- ```W``` : Representa a posição inicial de uma parede onde só o PacMan passa;
- ```E``` : Representa a posição inicial de uma parede onde só os fantasmas passam (spawn);
- ```1``` : Representa a posição inicial de um ponto que, ao ser comido, deixa o PacMan corajoso (veja mais abaixo);
- ```2``` : Representa a posição inicial de um ponto que, ao ser comido, deixa o PacMan rápido;
- Caso haja algum espaço em branco, será inicializada um ponto já comido (e, logo, invisível) naquela posição.


### 👟 Movimentação do PacMan
Ao iniciar o jogo, o PacMan possui velocidade zero. É somente a partir de se apertar uma tecla de movimento (as setas do teclado) que o personagem se moverá com uma velocidade constante. Depois de ter iniciado algum movimento, o Pacman nunca mais conseguirá ficar parado: excetuando-se o caso em que algo impeça seu movimento (como uma colisão, algo que será detalhado no próximo tópico), ele sempre possuirá velocidade para alguma das quatro direções definidas no enum **Direcoes**. Dessa forma, o jogador deverá estar constantemente fugindo dos seus inimigos e comendo pontos no mapa sem parar.

É importante ressaltar que o movimento de fato é definido na classe **PersonagemController** e, portanto, as classes **PacManController** e **FantasmaController** o herdarão. Assim, qualquer novo personagem que seja implementado futuramente poderá se mover por meio do mesmo método ao herdá-lo.

Em relação ao código, a dinâmica resumida é: durante o loop principal do jogo, é chamado a função *mover()* da classe **PacManController** que irá mudar a posição do PacMan no mapa constantemente baseado em uma velocidade (x,y) que a classe **PacManModel** herda da classe **GraficoModel**. Tal velocidade será sempre modificada toda vez que o jogador pressionar uma seta do teclado, pois uma instância da classe **MovimentoComSetas**, que implementa a interface **KeyListener**, está adicionada como *key listener* ao *frame* da janela do jogo principal.

### 💥 Colisão
O pac-man é capaz de colidir com outros objetos, como as paredes, os pontos e os fantasmas. Cada objeto define se o pac-man pode atravessá-lo ou não e define um comportamento para o sistema no evento da colisão. As classes responsáveis pelo tratamento de colisão são as que herdam de GraficoController.

### 🏎 Movimentação Automática dos Fantasmas
Os fantasmas possuem o mesmo estilo de movimentação automática utilizado no PacMan original. O mapa do jogo é dividido em pequenos quadrados de tamanho 16x16 pixels (que chamamos, no código, de Tile) de forma que cada objeto no mapa (uma parede, um fantasma ou até o próprio PacMan) possui uma posição (x,y) em unidades de Tile. Os fantasmas, em especial, possuem uma coordenada Tile (x,y) que representa o alvo para onde eles querem se mover. Esse alvo é determinado por diversos algoritmos diferentes que agem em momentos oportunos (mais detalhes abaixo). Determinada a coordenada-alvo, o fantasma checará todas as possíveis direções para onde ele pode se mover e escolherá sempre ir para aquela que o deixará mais próximo do alvo.

A determinação da coordenada-alvo depende de dois fatores: a situação e o tipo de fantasma. Existe, atualmente, no código, quatro situações possíveis: Ficar parado (SelfTarget), Sair da casinha (LeaveHouse), Perseguir (Chase) e dispersar (Scatter). As duas primeiras situações não estão implementadas no código porque, por enquanto, só temos um nível no jogo e elas são situações irrelevantes, visto que nenhum fantasma fica parado e todos devem sair da casinha imediatamente no inicio do jogo. Os dois últimos estão implementados  de modo a sempre se revezarem (em um momento, os fantasmas estão no modo Chase, em outros no modo Scatter).

O modo Scatter faz cada um dos quatro tipos de fantasma ir para seu respectivo canto do mapa e ficar rodando por lá. É, portanto, um modo que depende, também, do tipo de fantasma que foi instanciado visto que, para cada um deles, será determinada uma coordenada-alvo específica fora do mapa.

Já o modo Chase corresponde ao comportamento dos fantasmas de perseguir o PacMan. Os quatro tipos de fantasmas (Blinky, Pinky, Inky e Clyde) possuem um comportamento próprio: O Blinky terá como alvo a posição atual do PacMan, o Pinky tentará emboscar o PacMan colocando sua coordenada-alvo quatro tiles a frente da posição dele, o Inky usará a posição do PacMan e do Blinky para calcular seu alvo e também emboscar o PacMan (caso haja mais de um Blinky no jogo, espera-se que somente um seja usado como base) e, por fim, o Clyde age mais como um "criador de distração", visto que persegue o PacMan quando está longe dele, mas foge ao chegar perto para confundir o jogador.

A forma como isso foi feita é relativamente complexo e, por isso, sugerimos a leitura do **[artigo do site GameInternals](https://gameinternals.com/understanding-pac-man-ghost-behavior)** que foi usado como base para a criação desse comportamento.

É importante, por fim, ressaltar que os algoritmos de movimento automático dos fantasmas foram organizados de forma a utilizar o padrão Strategy.

### 🖼 Fábricas de Gráficos
O jogo suporta diferentes temas de gráficos que modificam os sprites de cada objeto do jogo de acordo com o tema escolhido. Por exemplo, temos o tema "Original", com os sprites originais do PacMan, e o tema "Computador", com sprites relacionados a computadores.

![Original](docs/aparencias.png)

Essas aparências diferentes são atingidas utilizando o padrão Fábrica Abstrata, já que para cada tema temos uma fábrica diferente, como a OriginalFactory ou a ComputadorFactory, que implementa uma fábrica abstrata de imagens. Cada fábrica dessas possui um Hashmap que mapeia o nome do sprite até o caminho de sua imagem, para facilitar a criação desses modelos no código.

### 🎢 Diferentes Fases
Foram construídas duas fases diferentes para o jogo, de forma que a segunda, com seus corredores mais fechados e tortuosos, fosse mais difícil que a primeira.

Essas fases foram implementadas utilizando o padrão de projeto [State](https://refactoring.guru/pt-br/design-patterns/state/java/example), no qual guardamos em um objeto ```fase``` a atual fase do jogo (seu estado), e utilizamos seus métodos no código principal de forma que ele atue diferentemente para cada fase.

### 💪 Diferentes Poderes
Diferentemente do PacMan original, no TakaMan, o jogador pode usar dois poderes diferentes (que são representados, no mapa, por "pontos comestíveis especiais", cujo gráfico depende do estilo de jogo escolhido). Há o poder original de tornar os inimigos assustados (eles ficam mais lentos e o PacMan pode comê-los) chamado aqui de "Coragem" e há o poder "Rapidez", que permite que o jogador tenha o dobro de velocidade por um tempo limitado.  

Por utilizarmos o padrão Decorator, é possível uma nova estratégia antes inexistente no PacMan original: o jogador pode combinar poderes de modo a ficar "poderoso" por mais tempo. Cada um dos especiais dura seis segundos e se o jogador pegar mais um especial antes desse tempo acabar, ele terá mais seis segundos de especial combinado (ou seja, ele poderá, nesse novo tempo, usufruir dos benefícios de ambos os poderes).

### 🔈 Som

A classe **Som** é responsável por lidar com o áudio que é tocado durante o jogo, seja este uma *música* ou um *efeito sonoro*. Esses dois tipos de áudio são tratados de forma diferente, já que as músicas precisam repetir e tocar ao longo de todo o jogo enquanto os efeitos sonoros são tocados apenas uma vez.

Para cada fase, bem como para o menu e para a tela de Game Over, possuímos uma música diferente, guardada na pasta *"res"*. No caso dos efeitos sonoros, cada um deles é associado a um evento específico do jogo, como perder uma vida ou comer um fantasma.

## 🏛 Arquitetura
O projeto foi arquitetado desde o princípio com o padrão de projeto **[MVC (Model–View–Controller)](https://pt.wikipedia.org/wiki/MVC)**, com o intuito de facilitar e modularizar o desenvolvimento do jogo.

![DiagramaDeClasses](/docs/DiagramaTakaMan.png)

## 🧅 Camadas de Classes
A arquitetura do projeto foi dividida em diversas ***camadas*** que interagem entre si por meio dos Controllers, como é possível ver no Diagrama de Classes. Cada camada lida com um aspecto diferente do projeto, e costuma ter sua própria divisão baseada no MVC.

As camadas de classes são as seguintes:

#### ▶ ExecutaJogo
Essa classe tem como responsabilidade criar o "looping" do jogo. Ela contém nosso método main, no qual o mapa e a janela do jogo são criados.

#### 💻 Jogo
A camada **Jogo** lida com os aspectos mais gerais do jogo, como a pontuação do jogador, a sequência de fases, a HUD, etc.

- **JogoController:**
    - Controla as partes do jogo em um nível mais alto;
    - Métodos:
        - *atualiza()* - atualiza todas as partes.
        - *atualizaMenu()* - atualiza o Menu do jogo.
        - *atualizaJogo()* - atualiza os elementos do jogo.
        - *criaFase()* - cria uma fase.
- **JogoModel:**
    - Atributos:
        - *vidas* - guarda as vidas do jogador;
        - *pontuacao* - guarda a pontuacao do jogador;
        - *fase* - guarda a fase atual do jogo;
        - *mapa* - guarda o mapa da fase atual.
- **JogoView:**
    - Herda da classe **Canvas**;
    - Atributos:
        - *hud* - guarda a classe HUD da interface do Jogo;
        - *bufferStrategy* - atributo referente à renderização do jogo na Janela.
    - Métodos:
        - *criaFrame()* - cria uma frame onde o resto da View irá renderizar;
        - *renderizaFrame()* - exibe a frame na Janela;
        - *criaBufferStrategy()* - cria a BufferStrategy do Canvas do jogo.
- **Menu:**
    - Lida com o Menu do jogo;
    - Métodos:
        - *getBotao()* - retorna o botão apertado;
        - *renderiza()* - renderiza o Menu;

#### 🗾 Mapa
A camada **Mapa** tem como responsabilidade armazenar o mapa criado e consultar os objetos que estejam em determinadas posições, para o tratamento de colisões em outras classes.

- **MapaController:**
    - Métodos:
        - *getObjetoNaPosicao()* - retorna todos os objetos que estão na posição definida.
- **MapaModel:**
    - Responsável por armazenar o mapa que será manipulado pela controller.
- **MapaView:**
    - Métodos:
        - *renderizar()* - renderiza na tela todos os objetos do mapa.

#### 📊 Grafico
A camada **Grafico** foi criada para representar os objetos do jogo: os Pontos, as Paredes e os Personagens. Ela possui as funcionalidades em comum desses objetos, como sua posição.

- **GraficoController:**
    - Atributos:
        - *model* - guarda uma instância da Model do Grafico;
        - *view* - guarda uma instância da View do Grafico.
    - Métodos:
        - *colidir()* - lida com a colisão do Grafico;
        - *atualizaView()* - atualiza a View do Grafico.
- **GraficoModel:**
    - Atributos:
        - *posicaoX* - guarda a posição do Grafico no eixo X;
        - *posicaoY* - guarda a posição do Grafico no eixo Y;
        - *velocidadeX* - guarda a velocidade do Grafico no eixo X;
        - *velocidadeY* - guarda a velocidade do Grafico no eixo Y;
        - *direcao* - guarda a direção para qual o Grafico está apontando.
- **GraficoView:**
    - Atributos:
        - *imagem* - guarda a imagem do Grafico;
        - *mapaDirecaoTransformacao* - um HashMap que mapeia a direção do Grafico na transformação que a imagem deve sofrer para apontar naquela direção.
    - Métodos:
        - *renderizar()* - renderiza a imagem do Grafico;
        - *rotacionarQuadrante()* - retorna a transformação de rotação;
        - *espelhar()* - retorna a transformação de espelhamento.

#### 🖌 Aparências

​	As animações foram feitas a partir do padrão de projeto Fábrica Abstrata, sendo assim as classes referentes aos modelos de gráficos são iguais, mudando apenas os caminhos para cada conjunto de imagens.

* **GraficoFactory:**
  * Métodos:
    * *criaImagem()* - gera animação a partir de um caminho;
    * *criaFonte()* - gera fonte a partir de um caminho.
* **OriginalFactory:**
  * Implementa **GraficoFactory**;
  * Atributos:
    * *mapaDeImagens* - guarda mapa com os caminhos das imagens do tema original do PacMan.
  
  * Métodos: 
    * *criaImagem()* - gera animação a partir de um caminho;
  * *criaFonte()* -gera fonte a partir de um caminho.
* **ComputadorFactory:**
* Implementa **GraficoFactory**;
  * Atributos:
    * *mapaDeImagens* - guarda mapa com caminhos das imagens  do tema Computador.
  * Métodos:
    * *criaImagem()* - gera animação a partir de um caminho;
    * *criaFonte()* - gera fonte a partir de um caminho.
* **NarutoFactory:**
  * Implementa **GraficoFactory**;
  * Atributos:
    * *mapaDeImagens* - guarda mapa com caminhos das imagens dos tema Naruto.
  * Métodos:
    * *criaImagem()* - gera animação a partir de um caminho;
    * *criaFonte()* - gera fonte a partir de um caminho.

#### ⭕ Ponto
A camada **Ponto** representa os pontos que o Pacman pode "comer" durante o jogo. Tais pontos podem ser pontos simples (em que o Pacman somente come e ganha uma pontuação) ou pontos especiais (que garantem ao Pacman um efeito especial de jogo).

- **PontoController:**
    - Herda do **GraficoController**;
    - Métodos:
        - *colidir()* - Responsável por lidar com a colisão do ponto com outros objetos;
        - *atualizaView()* - Responsável por atualizar a view do ponto dependendo do seu estado.
- **PontoModel:**
    - Herda do **GraficoModel**;
    - Atributos:
        - *comido* - Define se o ponto foi ou não comido;
        - *especial* - Define o especial, caso exista, que o ponto possuirá (ainda não implementado);
        - *pontuacao* - Define a pontuação que o ponto dará ao Pacman quando for comido.

#### 🧱 Parede
Esta camada representa as paredes do jogo com seus respectivos gráficos.

- **ParedeController:**
    - Herda de **GraficoController**.
- **ParedeModel:**
    - Herda de **GraficoModel**.

#### 👤 Personagem
A camada **Personagem** foi criada para representar os personagens do jogo, que inclui o PacMan e os Fantasmas. Dessa forma, ela é herdeira da camada **Grafico** e possui as funcionalidades em comum dos personagens, como movimentação.

- **PersonagemController:**
    - Herda do **GraficoController**;
    - Métodos:
        - *setMovimento()* - define o movimento do personagem;
        - *mover()* - atualiza a posição do personagem;
        - *posicaoValida()* - checa se a posição do personagem é válida.
- **PersonagemModel:**
    - Herda do **GraficoModel**.

#### 🕹 PacMan
A camada **PacMan** representa o personagem protagozinado pelo jogador, herdando assim a camada **Personagem**. Esta diz respeito tanto à colisões quanto ao estado do personagem (se está com poder ativo ou não).

- **PacManModel:**
    - Herda de **PersonagemModel**;
    - Atributos:
        - *poder* - valor inteiro que indica o estado do PacMan.
- **PacManController:**
    - Herda de **PersonagemController**;
    - Métodos:
        - *colidir()* - verifica a olisão do jogador.

#### 👻 Fantasma
A camada **Fantasma** representa o personagem inimigo do jogo e é herdeira da camada **Personagem**. Os fantasmas do jogo possuem uma mudança de estado que está representada nesta camada.

- **FantasmaModel:**
    - Herda de **PersonagemModel**;
    - Existem subclasses do FantasmaModel chamadas **BlinkyModel**, **PinkyModel**, **InkyModel** e **ClydeModel** para saber qual o tipo de fantasma está sendo instanciado.
    - Atributos:
        - *comivel* - o estado do inimigo.
- **FantasmaController:**
    - Herda de **PersonagemController**.

#### 🛠 Utils
- **Direcoes**
    - Enum que possui todas as direções em que o pacman pode se locomover.
- **LeitorDeMapa:**
    - Classe abstrata que possui as funcionalidades relacionadas a leitura de arquivos txt para gerar o mapa do jogo;
    - Métodos:
        - *criaMapa()* - Retorna um array bidimensional com instâncias de controllers de objetos que estão em determinada parte do mapa, dependendo da leitura do arquivo txt;
        - *lerArquivo()* - Faz leitura do arquivo txt e retorna um array bidimensional com os caracteres lidos de tal arquivo. É chamada pela *criaMapa()*;
        - *criaObjeto()* - Instancia e retorna os controllers dependendo dos argumentos passados. É chamada na *criaMapa()*;
- **MovimentoComSetas:** 
    - Implementa **KeyListener**;
    - Classe que implementa a identificação, em tempo de execução, das setas do teclado que o jogador está pressionando durante o jogo;
    - Métodos: 
        - *keyPressed()* - Define o movimento que será feito ao ser pressionado um determinado botão de seta do teclado.
- **Fase:** 
    - Interface do padrão State;
    - Contém métodos abstratos que exercem as funcionalidades de cada fase;
    - Atributos:
        - *proximaFase* - mapeia uma fase a sua conseguinte.
    - Métodos: 
        - *tocarMusica()* - Toca a música daquela fase.
        - *criaMapa()* - Cria o mapa daquela fase.
        - *getNumero()* - Retorna o número da fase.
- **Som:** 
    - Classe que lida com o áudio do jogo, como as músicas e os efeitos sonoros;
    - Atributos:
        - *mapaDeEfeitos* - mapeia eventos a efeitos sonoros.
        - *mapaDeMusicas* - mapeia fases a músicas.
    - Métodos: 
        - *tocarMusica()* - Toca uma música.
        - *tocarEfeito()* - Toca um efeito sonoro.
- **GerenciadorPoderes:** 
    - Classe abstrata com funções úteis para gerenciamento de poderes/comportamentos;
    - Métodos: 
        - *adicionarPoder()* - Adiciona um novo poder na lista de poderes de um personagem;
        - *gerarComportamento()* - Retorna um comportamento baseado na lista de poderes de um personagem;
        - *montarComportamento()* - Retorna um comportamento combinado usando o padrão Decorator.

#### 🎯 AutoTarget

- **AutoTarget:**
    - Classe abstrata que representa um algoritmo genérico de determinação do alvo de um fantasma;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*;
        - *getAutoTarget()*: Retorna um objeto com o algoritmo de determinação do alvo dependendo da situação.
- **SelfTarget:**
    - Herda do **AutoTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma de modo que ele fique dentro da casinha;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.
- **LeaveHouseTarget:**
    - Herda do **AutoTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma de modo que ele saia de dentro da casinha;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.
- **FrightenedTarget:**
    - Herda do **AutoTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma que quer fugir do PacMan;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.
- **ChaseTarget:**
    - Herda do **AutoTarget**
    - Classe abstrata que representa um algoritmo de determinação do alvo de um fantasma perseguir o PacMan;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*;
        - *getChaseTarget()*: Retorna um objeto com o algoritmo Chase de determinação do alvo dependendo de que tipo o *FantasmaModel* é.
- **ScatterTarget:**
    - Herda do **AutoTarget**
    - Classe abstrata que representa um algoritmo de determinação do alvo de um fantasma que se dispersa no mapa;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*;
        - *getScatterTarget()*: Retorna um objeto com o algoritmo Scatter de determinação do alvo dependendo de que tipo o *FantasmaModel* é.
- **BlinkyChaseTarget:**
    - Herda do **ChaseTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma Blinky perseguir o PacMan;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.
- **PinkyChaseTarget:**
    - Herda do **ChaseTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma Pinky perseguir o PacMan;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.
- **InkyChaseTarget:**
    - Herda do **ChaseTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma Inky perseguir o PacMan;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.
- **ClydeChaseTarget:**
    - Herda do **ChaseTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma Clyde perseguir o PacMan;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.
- **BlinkyScatterTarget:**
    - Herda do **ScatterTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma Blinky que se dispersa;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.
- **PinkyScatterTarget:**
    - Herda do **ScatterTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma Pinky que se dispersa;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.
- **InkyScatterTarget:**
    - Herda do **ScatterTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma Inky que se dispersa;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.
- **ClydeScatterTarget:**
    - Herda do **ScatterTarget**
    - Classe que representa um algoritmo de determinação do alvo de um fantasma Clyde que se dispersa;
    - Atributos:
        - *fantasmaModel*: o Model do fantasma que rodará o algoritmo;
    - Métodos:
        - *updateTarget()*: Atualiza as coordenadas-alvo de um *FantasmaModel*.

### 🎨 Decorators
- **Comportamento:**
    - Interface com as funções padrões que um comportamento de jogador deve ter;
    - Métodos:
        - *iniciar()*: Inicia o *Comportamento*;
        - *checarSeTerminou()*: É usada no loop principal para checar se o comportamento terminou.
- **ComportamentoBase:**
    - Implementa **Comportamento**
    - Classe que representa um comportamento base de um jogador;
    - Métodos:
        - *iniciar()*: Inicia o *Comportamento*;
        - *checarSeTerminou()*: É usada no loop principal para checar se o comportamento terminou.
- **ComportamentoDecorator:**
    - Implementa **Comportamento**
    - Classe que representa o decorator de comportamentos modificados;
    - Atributos:
        - *comportamento*: comportamento referenciado pelo Decorator;
    - Métodos:
        - *iniciar()*: Inicia o *Comportamento*;
        - *checarSeTerminou()*: É usada no loop principal para checar se o comportamento terminou;
        - *retornaEstadoInicial()*: Retorna o jogador para seu estado inicial (sem nenhum poder);
        - *alternaFantasmas()*: Alterna o algoritmo dos fantasmas dependendo do comportamento do jogador;
        - *podeMudarVel()*: Checa se o jogador está em condições de mudar sua velocidade.
- **CoragemDecorator:**
    - Herda de **ComportamentoDecorator**
    - Classe que representa o comportamento "Coragem";
    - Atributos:
        - *tempo*: contagem de tempo de especial decorrido;
        - *tempoAnterior*: contagem anterior (última checagem) de tempo de especial decorrido;
    - Métodos:
        - *iniciar()*: Inicia o *Comportamento*;
        - *checarSeTerminou()*: É usada no loop principal para checar se o comportamento terminou.
- **RapidezDecorator:**
    - Herda de **ComportamentoDecorator**
    - Classe que representa o comportamento "Coragem";
    - Atributos:
        - *tempo*: contagem de tempo de especial decorrido;
        - *tempoAnterior*: contagem anterior (última checagem) de tempo de especial decorrido;
    - Métodos:
        - *iniciar()*: Inicia o *Comportamento*;
        - *checarSeTerminou()*: É usada no loop principal para checar se o comportamento terminou;
        - *ficaMaisRapido()*: Faz o jogador ficar mais rápido.