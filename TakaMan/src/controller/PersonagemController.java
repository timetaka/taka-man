package controller;

import java.util.ArrayList;

import model.PersonagemModel;
import util.Direcoes;
import util.Tile;

public abstract class PersonagemController extends GraficoController {

	public void setMovimento(Direcoes direcao) {
		model.setDirecao(direcao);
		switch (direcao) {
		case DIREITA:
			model.setVelocidadeX(model.getVelocidadePadrao());
			model.setVelocidadeY(0);
			break;
		case ESQUERDA:
			model.setVelocidadeX((-1) * model.getVelocidadePadrao());
			model.setVelocidadeY(0);
			break;
		case CIMA:
			model.setVelocidadeX(0);
			model.setVelocidadeY((-1) * model.getVelocidadePadrao());
			break;
		case BAIXO:
			model.setVelocidadeX(0);
			model.setVelocidadeY(model.getVelocidadePadrao());
			break;
		}
	}

	public boolean mover() {
		int novaPosicaoX = model.getPosicaoX();
		int novaPosicaoY = model.getPosicaoY();

		novaPosicaoX += model.getVelocidadeX();
		novaPosicaoY += model.getVelocidadeY();

		if (novaPosicaoX > 808) {
			novaPosicaoX = 488;
		} else if (novaPosicaoX < 488) {
			novaPosicaoX = 808;
		}

		if (!posicaoValida(novaPosicaoX, novaPosicaoY)) {
			return false;
		}

		ArrayList<GraficoController> objetosColididos = MapaController.getObjetoNaPosicao(novaPosicaoX, novaPosicaoY);

		if (colidir(objetosColididos)) {
			this.model.setPosicaoX(novaPosicaoX);
			this.model.setPosicaoY(novaPosicaoY);
			this.atualizaParedes();
			return true;
		}

		this.atualizaParedes();
		
		return false;
	}

	protected boolean abriuParede(Direcoes direcao) {
		PersonagemModel castedModel;
		if (this.model instanceof PersonagemModel) {
			castedModel = (PersonagemModel) this.model;
		} else {
			return false;
		}

		if (castedModel.getTemParede(direcao)) {
			return false;
		}

		return (castedModel.getTemParede(direcao) != castedModel.getTinhaParede(direcao));
	}

	protected boolean abriuParede() {
		boolean abriuParede = false;
		for (Direcoes direcao: Direcoes.values()) {
			abriuParede |= this.abriuParede(direcao);
		}

		return abriuParede;
	}

	private boolean posicaoValida(int novaPosicaoX, int novaPosicaoY) {
		if (novaPosicaoX < 0
				|| novaPosicaoY < 0 /* || NovaPosicaoX > VALOR_MAXIMO_X || NovaPosicaoY > VALOR_MAXIMO_Y */)
			return false;
		return true;
	}

	private void atualizaParedes() {
		PersonagemModel castedModel;
		if (this.model instanceof PersonagemModel) {
			castedModel = (PersonagemModel) this.model;
		} else {
			return;
		}

		boolean temParede;
		ArrayList<GraficoController> objetos;
		boolean centroX = castedModel.getPosicaoX() == Tile.centroTileParaPixel(castedModel.getTileX());
		boolean centroY = castedModel.getPosicaoY() == Tile.centroTileParaPixel(castedModel.getTileY());

		objetos = MapaController.getObjetoNaPosicaoTile(this.model.getTileX(), this.model.getTileY() - 1);
		if (objetos.isEmpty()) {
			temParede = false;
		} else {
			temParede = objetos.get(0) instanceof ParedeController;
		}
		castedModel.setTemParede(temParede || !centroX, Direcoes.CIMA);

		objetos = MapaController.getObjetoNaPosicaoTile(this.model.getTileX(), this.model.getTileY() + 1);
		if (objetos.isEmpty()) {
			temParede = false;
		} else {
			temParede = objetos.get(0) instanceof ParedeController;
		}
		castedModel.setTemParede(temParede || !centroX, Direcoes.BAIXO);

		objetos = MapaController.getObjetoNaPosicaoTile(this.model.getTileX() + 1, this.model.getTileY());
		if (objetos.isEmpty()) {
			temParede = false;
		} else {
			temParede = objetos.get(0) instanceof ParedeController;
		}
		castedModel.setTemParede(temParede || !centroY, Direcoes.DIREITA);

		objetos = MapaController.getObjetoNaPosicaoTile(this.model.getTileX() - 1, this.model.getTileY());
		if (objetos.isEmpty()) {
			temParede = false;
		} else {
			temParede = objetos.get(0) instanceof ParedeController;
		}
		castedModel.setTemParede(temParede || !centroY, Direcoes.ESQUERDA);
	}
}
