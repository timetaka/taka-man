package controller;

import java.util.ArrayList;

import util.GerenciadorPoderes.PODERES;
import view.GraficoFactory;

public class MapaController {

	private static ArrayList<GraficoController> mapa = new ArrayList<>();

	public static ArrayList<GraficoController> getObjetoNaPosicao(int posicaoX, int posicaoY) {

		ArrayList<GraficoController> objetosColididos = new ArrayList<>();

		for (GraficoController objeto : mapa) {
			boolean estaColidindo = false;

			if(posicaoX + 16 > objeto.getModel().getPosicaoX() && posicaoX - 16 < objeto.getModel().getPosicaoX()) {
				if(posicaoY + 16 > objeto.getModel().getPosicaoY() && posicaoY - 16 < objeto.getModel().getPosicaoY()) {
					estaColidindo = true;
				}
			}

			if (estaColidindo) {
				objetosColididos.add(objeto);
			}

		}
		return objetosColididos;
	}
	
	public static ArrayList<GraficoController> getObjetoNaPosicaoTile(int tileX, int tileY) {

		ArrayList<GraficoController> objetosColididos = new ArrayList<>();

		for (GraficoController objeto : mapa) {
			boolean estaColidindo = false;

			if (tileX == objeto.getModel().getTileX() && tileY == objeto.getModel().getTileY()) {
				estaColidindo = true;
			}

			if (estaColidindo) {
				objetosColididos.add(objeto);
			}

		}
		
		return objetosColididos;
	}

	public static void resetarMapa() {
		for (GraficoController objeto : mapa) {
			objeto.resetarPosicao();
		}
	}

	public static void setMapa(GraficoController[][] novoMapa, GraficoFactory fabricaDeGraficos) {
		mapa = new ArrayList<GraficoController>();

		for (GraficoController[] graficoRow : novoMapa) {
			for (GraficoController grafico : graficoRow) {
				mapa.add(grafico);
				if (grafico instanceof PacManController || grafico instanceof FantasmaController) {
					mapa.add(new PontoController(grafico.getModel().getPosicaoX(), grafico.getModel().getPosicaoY(), true, fabricaDeGraficos.criaImagem("Ponto"), PODERES.INERTE));
				}
			}
		}
	}

	public static ArrayList<GraficoController> getMapa() {
		return mapa;
	}

}
