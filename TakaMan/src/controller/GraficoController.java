package controller;

import java.awt.Graphics2D;
import java.util.ArrayList;

import model.GraficoModel;
import view.GraficoView;

public abstract class GraficoController {
	
	protected GraficoModel model;
	protected GraficoView view;
	
	public GraficoView getView() {
		return view;
	}

	public void atualizaView(Graphics2D frame) {
		int x = model.getPosicaoX();
		int y = model.getPosicaoY();
		view.renderizar(frame, x, y, model.getDirecao());
	}
	
	public GraficoModel getModel() {
		return this.model;
	}

	public boolean colidir(ArrayList<GraficoController> objetosColididos) {
		// o false significa que o objeto eh intransponivel
		// por padrao, os objetos serao intransponiveis
		return false;
	}

	public void resetarPosicao() {
		this.model.setPosicaoX(this.model.getPosicaoInicialX());
		this.model.setPosicaoY(this.model.getPosicaoInicialY());
	}
}
