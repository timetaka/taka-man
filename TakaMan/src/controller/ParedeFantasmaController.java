package controller;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import model.ParedeModel;
import view.GraficoView;

public class ParedeFantasmaController extends GraficoController {

	public ParedeFantasmaController(int posX, int posY, ArrayList<BufferedImage> bufferedImage) {
		this.model = new ParedeModel(posX, posY);
		this.view = new GraficoView(bufferedImage);
	}
	
	public void atualizaView(Graphics2D frame) {
		//nao faz nada porque eh invisivel
	}
	
	public boolean colidir(ArrayList<GraficoController> objetosColididos) {
		boolean transpor = true;
		
		for (GraficoController objeto : objetosColididos) {
			if(objeto instanceof FantasmaController)
				transpor = false;
		}
		
		return transpor;
	}

}
