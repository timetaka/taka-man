package controller;

import java.util.ArrayList;
import java.util.Iterator;

import controller.Menu.BOTOES;
import model.BlinkyModel;
import model.JogoModel;

import java.awt.Graphics2D;

import util.Fase;
import util.Fase1;
import util.LeitorDeMapa;
import util.MovimentoComSetas;
import util.Som;
import view.JogoView;
import view.NarutoFactory;
import view.GraficoFactory;
import view.OriginalFactory;
import view.ComputadorFactory;

public class JogoController {
	private static final int TEMPO_MODO_FANTASMAS = 10000;
	
	private static Menu menu;
	private static JogoModel jogoModel;
	private static JogoView jogoView;
	private static Graphics2D frame;
	
	private static GraficoFactory fabricaDeGraficos = null;
	private static GraficoController[][] mapa = null;
	private static PacManController pacManController = null;
	private static FantasmaController blinkyController = null;
	private static ArrayList<FantasmaController> fantasmaControllers = null;
	
	private static long tempo = 0;
	private static long tempoAnterior;

	public enum ESTADO {
		Menu,
		Visual,
		Jogo,
		Reset,
		GameOver,
		Victory
	};

	private static ESTADO estadoDoJogo = ESTADO.Menu;

	public JogoController() {
		menu = new Menu();
		jogoModel = new JogoModel(3, 500, new Fase1(), null);
		jogoView = new JogoView(menu);
		jogoView.criaBufferStrategy();
		tempoAnterior = System.currentTimeMillis();
	}

	public void atualiza() {
		if (estadoDoJogo == ESTADO.Menu || estadoDoJogo == ESTADO.Visual) {
			Som.tocarMusica(0);
			jogoModel.setFase(new Fase1());
			jogoModel.setVidas(3);
			jogoModel.setPontuacao(0);
			atualizaMenu();
		} else if (estadoDoJogo == ESTADO.Reset) {
			jogoModel.setVidas(3);
			criaFase(fabricaDeGraficos);
			estadoDoJogo = ESTADO.Jogo;
		} else if (estadoDoJogo == ESTADO.GameOver || estadoDoJogo == ESTADO.Victory) {
			long tempoInicial = System.currentTimeMillis();
			while (System.currentTimeMillis() - tempoInicial < 6000) {
				atualizaMenu();
			}
			estadoDoJogo = ESTADO.Menu;
		} else {
			atualizaJogo();
			tempo = tempo + System.currentTimeMillis() - tempoAnterior;
			tempoAnterior = System.currentTimeMillis();
		}
	}

	private static void atualizaMenu() {
		frame = jogoView.criaFrame();
		menu.renderiza(frame, estadoDoJogo);
		BOTOES botao = menu.getBotao(estadoDoJogo);
		if (botao == BOTOES.Jogar) {
			estadoDoJogo = ESTADO.Visual;
		} else if (botao == BOTOES.Sair) {
			System.exit(1);
		} else if (botao == BOTOES.Original) {
			fabricaDeGraficos = new OriginalFactory();
			criaFase(fabricaDeGraficos);
			estadoDoJogo = ESTADO.Jogo;
		} else if (botao == BOTOES.Computador) {
			// substituir pelo ComputadorFactory
			fabricaDeGraficos = new ComputadorFactory();
			criaFase(fabricaDeGraficos);
			estadoDoJogo = ESTADO.Jogo;
		} else if (botao == BOTOES.Naruto) {
			fabricaDeGraficos = new NarutoFactory();
			criaFase(fabricaDeGraficos);
			estadoDoJogo = ESTADO.Jogo;
		} 
		else if (botao == BOTOES.Voltar) {
			estadoDoJogo = ESTADO.Menu;
		}
		
		jogoView.renderizaMenu(frame);
	}

	private static void criaFase(GraficoFactory fabricaDeGraficos) {
		fantasmaControllers = new ArrayList<>();
		jogoModel.setPontos(0);
		mapa = jogoModel.getFase().criaMapa(fabricaDeGraficos);

		for (GraficoController[] graficoRow: mapa) {
			for (GraficoController grafico: graficoRow) {
				if (grafico instanceof PacManController) {
					pacManController = (PacManController) grafico;
				}
				
				if (grafico instanceof FantasmaController) {
					fantasmaControllers.add((FantasmaController) grafico);
					if (grafico.getModel() instanceof BlinkyModel) {
						blinkyController = (FantasmaController) grafico;
						// IMPORTANTE: Se tiver mais de um blinky, os inkys v�o usar s� um para sua logica
					}
				}
			}
		}

		jogoView.criaMovimentoComSetas(new MovimentoComSetas(pacManController));

		MapaController.setMapa(mapa, fabricaDeGraficos);
		
		jogoView.setGraficos(fabricaDeGraficos);
	}

	private static void atualizaJogo() {
		jogoModel.getFase().tocarMusica();
		boolean alternarMovimentacao = false;
		
		frame = jogoView.criaFrame();

		pacManController.mover();
		pacManController.getComportamento().checarSeTerminou(pacManController);
		
		Iterator<FantasmaController> fantasmaIterator = fantasmaControllers.iterator();
		
		if(tempo > TEMPO_MODO_FANTASMAS) {
			tempo = 0;
			alternarMovimentacao = true;
		}

		while(fantasmaIterator.hasNext()) {
			FantasmaController fantasmaController = fantasmaIterator.next();
			
			if(alternarMovimentacao) {
				fantasmaController.alternaModoMovimentacao();
			}			
			fantasmaController.mover();
		}
		
		jogoView.renderizaJogo(frame);

		if (jogoModel.getPontos() <= 0) {
			if (jogoModel.getFase().getClass() == jogoModel.getFaseFinal()) {
				Som.tocarMusica(0);
				estadoDoJogo = ESTADO.Victory;
			} else {
				jogoModel.setFase(Fase.proximaFase.get(jogoModel.getFase().getClass()));
				estadoDoJogo = ESTADO.Reset;
			}
		}

		if (!pacManController.getVivo()) {
			pacManController.setVivo(true);
			int vidas = jogoModel.getVidas() - 1;
			jogoModel.setVidas(vidas);

			if (vidas >= 0) {
				MapaController.resetarMapa();
			} else {
				Som.tocarMusica(-1);
				estadoDoJogo = ESTADO.GameOver;
			}
		}
	}
	
	public static FantasmaController getBlinkyController() {
		return blinkyController;
	}
	
	public static PacManController getPacManController() {
		return pacManController;
	}

	public static JogoModel getJogoModel() {
		return jogoModel;
	}
	
	public static Iterator<FantasmaController> getFantasmasControllers() {
		return fantasmaControllers.iterator();
	}
	
	public static GraficoFactory getFabricaDeGraficos() {
		return fabricaDeGraficos;
	}

}
