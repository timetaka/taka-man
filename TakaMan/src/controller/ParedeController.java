package controller;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import model.ParedeModel;
import view.GraficoView;

public class ParedeController extends GraficoController {

	public ParedeController(int posX, int posY, ArrayList<BufferedImage> bufferedImage) {
		this.model = new ParedeModel(posX, posY);
		this.view = new GraficoView(bufferedImage);
	}

}
