package controller;

import javax.swing.JFrame;

import org.junit.jupiter.api.Test;

import util.Direcoes;
import util.MovimentoComSetas;

class PacManControllerTest {	
	@Test
	void testarColisao() {
		/*PacManController pacmanController = new PacManController(1, 1, null);
		PontoModel pontoModel = new PontoModel(1, 2, false);
		ParedeModel paredeMovel = new ParedeModel(2, 1);
		
		pacmanController.colidir(paredeMovel);
		pacmanController.colidir(pontoModel);*/
	}
	
	@Test
	void testarMovimento() {
		PacManController pacmanController = new PacManController(1, 1, null);

		pacmanController.setMovimento(Direcoes.CIMA);
		pacmanController.mover();
		
		pacmanController.setMovimento(Direcoes.BAIXO);
		pacmanController.mover();
		
		pacmanController.setMovimento(Direcoes.BAIXO);
		pacmanController.mover();
		
		pacmanController.setMovimento(Direcoes.DIREITA);
		pacmanController.mover();
	}
	
	@Test
	void testarMovimentoComSetas() {
		PacManController pacmanController = new PacManController(1, 1, null);
		MovimentoComSetas mcs = new MovimentoComSetas(pacmanController);
		
		JFrame janela = new JFrame();
		janela.addKeyListener(mcs);
		janela.setSize(500, 500);        
		janela.setTitle("Teste de Movimento com Setas");
		janela.setVisible(true);
		while(true) {
			
		}
	}

}
