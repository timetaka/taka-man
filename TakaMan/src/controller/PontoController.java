package controller;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import model.PontoModel;
import util.Som;
import util.Som.EFEITO;
import util.GerenciadorPoderes;
import util.GerenciadorPoderes.PODERES;
import view.GraficoView;

public class PontoController extends GraficoController {

	public PontoController(int posX, int posY, boolean comido, ArrayList<BufferedImage> arrayList, GerenciadorPoderes.PODERES poder) {
		this.model = new PontoModel(posX, posY, comido, poder);
		this.view = new GraficoView(arrayList);
	}

	@Override
	public boolean colidir(ArrayList<GraficoController> objetosColididos) {

		if (objetosColididos.size() > 0) {
			for (GraficoController objeto : objetosColididos) {
				if (objeto instanceof PacManController) {
					PontoModel model = (PontoModel) this.model;

					if (!model.isComido()) {
						int pontuacaoAtual = JogoController.getJogoModel().getPontuacao();
						JogoController.getJogoModel().setPontuacao(pontuacaoAtual + 10);
						JogoController.getJogoModel().setPontos(JogoController.getJogoModel().getPontos() - 1);
						Som.tocarEfeito(EFEITO.WAKA);

						if (model.getPoder() != PODERES.INERTE) {
							JogoController.getPacManController().adicionarPoder(model.getPoder());
						}
					}

					model.setComido(true);
					this.model = model;

				}
			}
		}

		// true significa que o objeto eh transponivel
		return true;
	}

	@Override
	public void atualizaView(Graphics2D frame) {
		PontoModel model = (PontoModel) this.model;
		if (!model.isComido()) {
			super.atualizaView(frame);
		}
	}

}
