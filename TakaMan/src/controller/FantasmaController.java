package controller;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import model.BlinkyModel;
import model.ClydeModel;
import model.FantasmaModel;
import model.InkyModel;
import model.PinkyModel;
import util.Direcoes;
import util.Fantasmas;
import util.Tile;
import util.TargetModes;
import view.GraficoView;

public class FantasmaController extends PersonagemController {
	
	private boolean estaForaDoSpawn;
	private ArrayList<BufferedImage> imagens;

	public FantasmaController(int posX, int posY, boolean comivel, ArrayList<BufferedImage> imagens, Fantasmas fantasma) {
		estaForaDoSpawn = false;
		switch(fantasma) {
		case BLINKY:
			this.model = new BlinkyModel(posX, posY, comivel);
			break;
		case CLYDE:
			this.model = new ClydeModel(posX, posY, comivel);
			break;
		case INKY:
			this.model = new InkyModel(posX, posY, comivel);
			break;
		case PINKY:
			this.model = new PinkyModel(posX, posY, comivel);
			break;
		}
		
		this.view = new GraficoView(imagens);
		this.imagens = imagens;
	}
	
	public void autoMover() {
		FantasmaModel castedModel;		
		if (this.model instanceof FantasmaModel) {
			castedModel = (FantasmaModel) this.model;
		} else {
			return;
		}
		
		castedModel.getAutoTarget().updateTarget();
		
		if (castedModel.isMoving()) {
			if (this.abriuParede()) {
				castedModel.setMoving(false);
			}
		}
		
		if (castedModel.isMoving() == false) {
			if (castedModel.getAlvoTileX() < 0 || castedModel.getAlvoTileY() < 0) {
				castedModel.setVelocidadePadrao(1);
				fugirDoAlvo(castedModel);
			} else {
				castedModel.setVelocidadePadrao(2);
				cacarOAlvo(castedModel);
			}
		}
	}
	
	@Override
	public boolean colidir(ArrayList<GraficoController> objetosColididos) {
		ArrayList<GraficoController> fantasma = new ArrayList<GraficoController>();
		fantasma.add(this);
		
		boolean transpor = true;
		
		// a colisao do fantasma testa se ele pode transpor os outros objetos
		// por isso, para cada colisao que tenha acontecido ao mesmo tempo, � testado se o objeto � transponivel ou nao
		for (GraficoController objeto : objetosColididos) {
			if(!(objeto instanceof FantasmaController)) {
				transpor = transpor && objeto.colidir(fantasma);
			}
		}
		
		return transpor;
	}
	
	@Override
	public boolean mover() {
		this.autoMover();
		boolean moving = super.mover();

		FantasmaModel castedModel;
		if (this.model instanceof FantasmaModel) {
			castedModel = (FantasmaModel) this.model;
			castedModel.setMoving(moving);
		}

		return moving;
	}
	

	public boolean getEstaForaDoSpawn() {
		return estaForaDoSpawn;
	}
	
	public void setEstaForaDoSpawn(boolean estaForaCasinha) {
		this.estaForaDoSpawn = estaForaCasinha;
	}
	
	public void voltarParaCasinha() {
		this.estaForaDoSpawn = false;
		this.model.setPosicaoX(this.model.getPosicaoInicialX());
		this.model.setPosicaoY(this.model.getPosicaoInicialY());
	}
	
	public void alternaModoMovimentacao() {
		FantasmaModel castedModel = (FantasmaModel) this.model;
		
		if (castedModel.getTargetMode() == TargetModes.CHASE) {
			castedModel.setTargetMode(TargetModes.SCATTER);
		} else if (castedModel.getTargetMode() == TargetModes.SCATTER) {
			castedModel.setTargetMode(TargetModes.CHASE);
		}
	}
	
	public void definirComportamentoMedo(boolean comMedo) {
		FantasmaModel castedModel = (FantasmaModel) this.model;
		castedModel.setComivel(comMedo);
		
		if (comMedo) {
			castedModel.setTargetMode(TargetModes.FRIGHTENED);
			this.view = new GraficoView(JogoController.getFabricaDeGraficos().criaImagem("Fugitivo"));
		} else {
			castedModel.setTargetMode(castedModel.getAntigoTargetMode());
			this.view = new GraficoView(this.imagens);
		}
		
		
	}
	
	
	private void cacarOAlvo(FantasmaModel fantasmaModel) {
		int centroTileAlvoX = Tile.centroTileParaPixel(fantasmaModel.getAlvoTileX());
		int centroTileAlvoY = Tile.centroTileParaPixel(fantasmaModel.getAlvoTileY());
		
		int distanciaDefinitiva = -1;
		Direcoes direcaoDefinitiva = fantasmaModel.getDirecao();
		
		if (!fantasmaModel.getTemParede(Direcoes.CIMA) && fantasmaModel.getDirecao() != Direcoes.BAIXO) {
			int distancia = (centroTileAlvoX - fantasmaModel.getPosicaoX()) * (centroTileAlvoX - fantasmaModel.getPosicaoX());
			distancia += (centroTileAlvoY - (fantasmaModel.getPosicaoY() - 16)) * (centroTileAlvoY - (fantasmaModel.getPosicaoY() - 16));
			
			if (distancia < distanciaDefinitiva || distanciaDefinitiva == -1) {
				distanciaDefinitiva = distancia;
				direcaoDefinitiva = Direcoes.CIMA;
			}
		}
		
		if (!fantasmaModel.getTemParede(Direcoes.ESQUERDA) && fantasmaModel.getDirecao() != Direcoes.DIREITA) {
			int distancia = (centroTileAlvoX - (fantasmaModel.getPosicaoX() - 16)) * (centroTileAlvoX - (fantasmaModel.getPosicaoX() - 16));
			distancia += (centroTileAlvoY - fantasmaModel.getPosicaoY()) * (centroTileAlvoY - fantasmaModel.getPosicaoY());
			
			if (distancia < distanciaDefinitiva || distanciaDefinitiva == -1) {
				distanciaDefinitiva = distancia;
				direcaoDefinitiva = Direcoes.ESQUERDA;
			}
		}
		
		if (!fantasmaModel.getTemParede(Direcoes.BAIXO) && fantasmaModel.getDirecao() != Direcoes.CIMA) {
			int distancia = (centroTileAlvoX - fantasmaModel.getPosicaoX()) * (centroTileAlvoX - fantasmaModel.getPosicaoX());
			distancia += (centroTileAlvoY - (fantasmaModel.getPosicaoY() + 16)) * (centroTileAlvoY - (fantasmaModel.getPosicaoY() + 16));
			
			if (distancia < distanciaDefinitiva || distanciaDefinitiva == -1) {
				distanciaDefinitiva = distancia;
				direcaoDefinitiva = Direcoes.BAIXO;
			}
		}
		
		if (!fantasmaModel.getTemParede(Direcoes.DIREITA) && fantasmaModel.getDirecao() != Direcoes.ESQUERDA) {
			int distancia = (centroTileAlvoX - (fantasmaModel.getPosicaoX() + 16)) * (centroTileAlvoX - (fantasmaModel.getPosicaoX() + 16));
			distancia += (centroTileAlvoY - fantasmaModel.getPosicaoY()) * (centroTileAlvoY - fantasmaModel.getPosicaoY());
			
			if (distancia < distanciaDefinitiva || distanciaDefinitiva == -1) {
				distanciaDefinitiva = distancia;
				direcaoDefinitiva = Direcoes.DIREITA;
			}
		}
		
		if (distanciaDefinitiva != -1) {
			this.setMovimento(direcaoDefinitiva);
		}
	}
	
	private void fugirDoAlvo(FantasmaModel fantasmaModel) {
		int centroTileAlvoX = Tile.centroTileParaPixel((-1) * fantasmaModel.getAlvoTileX());
		int centroTileAlvoY = Tile.centroTileParaPixel((-1) * fantasmaModel.getAlvoTileY());
		
		int distanciaDefinitiva = -1;
		Direcoes direcaoDefinitiva = fantasmaModel.getDirecao();
		
		if (!fantasmaModel.getTemParede(Direcoes.CIMA)) {
			if (fantasmaModel.getDirecao() != Direcoes.BAIXO || fantasmaModel.isDirecaoReversivel()) {
				int distancia = (centroTileAlvoX - fantasmaModel.getPosicaoX()) * (centroTileAlvoX - fantasmaModel.getPosicaoX());
				distancia += (centroTileAlvoY - (fantasmaModel.getPosicaoY() - 16)) * (centroTileAlvoY - (fantasmaModel.getPosicaoY() - 16));
				
				if (distancia > distanciaDefinitiva || distanciaDefinitiva == -1) {
					distanciaDefinitiva = distancia;
					direcaoDefinitiva = Direcoes.CIMA;
				}
			}

		}
		
		if (!fantasmaModel.getTemParede(Direcoes.ESQUERDA)) {
			if (fantasmaModel.getDirecao() != Direcoes.DIREITA || fantasmaModel.isDirecaoReversivel()) {
				int distancia = (centroTileAlvoX - (fantasmaModel.getPosicaoX() - 16)) * (centroTileAlvoX - (fantasmaModel.getPosicaoX() - 16));
				distancia += (centroTileAlvoY - fantasmaModel.getPosicaoY()) * (centroTileAlvoY - fantasmaModel.getPosicaoY());
				
				if (distancia > distanciaDefinitiva || distanciaDefinitiva == -1) {
					distanciaDefinitiva = distancia;
					direcaoDefinitiva = Direcoes.ESQUERDA;
				}
			}

		}
		
		if (!fantasmaModel.getTemParede(Direcoes.BAIXO)) {
			if (fantasmaModel.getDirecao() != Direcoes.CIMA || fantasmaModel.isDirecaoReversivel()) {
				int distancia = (centroTileAlvoX - fantasmaModel.getPosicaoX()) * (centroTileAlvoX - fantasmaModel.getPosicaoX());
				distancia += (centroTileAlvoY - (fantasmaModel.getPosicaoY() + 16)) * (centroTileAlvoY - (fantasmaModel.getPosicaoY() + 16));
				
				if (distancia > distanciaDefinitiva || distanciaDefinitiva == -1) {
					distanciaDefinitiva = distancia;
					direcaoDefinitiva = Direcoes.BAIXO;
				}
			}
			
		}
		
		if (!fantasmaModel.getTemParede(Direcoes.DIREITA)) {
			if (fantasmaModel.getDirecao() != Direcoes.ESQUERDA || fantasmaModel.isDirecaoReversivel()) {
				int distancia = (centroTileAlvoX - (fantasmaModel.getPosicaoX() + 16)) * (centroTileAlvoX - (fantasmaModel.getPosicaoX() + 16));
				distancia += (centroTileAlvoY - fantasmaModel.getPosicaoY()) * (centroTileAlvoY - fantasmaModel.getPosicaoY());
				
				if (distancia > distanciaDefinitiva || distanciaDefinitiva == -1) {
					distanciaDefinitiva = distancia;
					direcaoDefinitiva = Direcoes.DIREITA;
				}	
			}
			
		}
		
		if (distanciaDefinitiva != -1) {
			this.setMovimento(direcaoDefinitiva);
		}
		
		fantasmaModel.setDirecaoReversivel(false);
	}
}
