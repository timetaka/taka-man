package controller;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.imageio.ImageIO;

import controller.JogoController.ESTADO;
import view.OriginalFactory;

import java.io.IOException;

public class Menu extends MouseAdapter {

    private int mx = 0;
    private int my = 0;

    private OriginalFactory fabricaDeGraficos = new OriginalFactory();
    private BufferedImage titulo;
    private BufferedImage fundo;
    private BufferedImage jogar;
    private BufferedImage sair;
    private BufferedImage visual;
    private BufferedImage original;
    private BufferedImage computador;
    private BufferedImage naruto;
    private BufferedImage voltar;

    public enum BOTOES {
        Nenhum,
        Jogar,
        Sair,
        Original,
        Computador,
        Naruto,
        Voltar
    }

    private BOTOES botao = BOTOES.Nenhum;

    public Menu() {
        try {
            titulo = ImageIO.read(getClass().getResource("/menu/titulo.png"));
            fundo = ImageIO.read(getClass().getResource("/menu/menu.png"));
            jogar = ImageIO.read(getClass().getResource("/menu/jogar.png"));
            sair = ImageIO.read(getClass().getResource("/menu/sair.png"));
            visual = ImageIO.read(getClass().getResource("/menu/visual.png"));
            original = ImageIO.read(getClass().getResource("/menu/original.png"));
            computador = ImageIO.read(getClass().getResource("/menu/computador.png"));
            naruto = ImageIO.read(getClass().getResource("/menu/naruto.png"));
            voltar = ImageIO.read(getClass().getResource("/menu/voltar.png"));
        } catch (IOException e) {
            System.out.println("Imagens do Menu Nao Encontradas");
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mx = e.getX();
        my = e.getY();
    }

    private boolean mouseOver(int mx, int my, int x, int y, int width, int height) {
        if (mx > x && mx < x + width) {
            if (my > y && my < y + height) {
                return true;
            }
        }
        return false;
    }

    public BOTOES getBotao(ESTADO estadoDoJogo) {
        if (estadoDoJogo == ESTADO.Menu) {
            if (mouseOver(mx, my, 400, 350, 300, 100)) {
                botao = BOTOES.Jogar;
            } else if (mouseOver(mx, my, 400, 450, 300, 100)) {
                botao = BOTOES.Sair;
            } else {
                botao = BOTOES.Nenhum;
            }
        } else if (estadoDoJogo == ESTADO.Visual) {
            if (mouseOver(mx, my, 400, 325, 300, 75)) {
                botao = BOTOES.Original;
            } else if (mouseOver(mx, my, 400, 425, 300, 75)) {
                botao = BOTOES.Computador;
            } else if (mouseOver(mx, my, 400, 550, 300, 100)) {
                botao = BOTOES.Voltar;
            } else if (mouseOver(mx, my, 530, 470, 240, 100)){
            	botao = BOTOES.Naruto;
            	System.out.println("botao naruto");
            } else {
                botao = BOTOES.Nenhum;
            }
        } else {
            botao = BOTOES.Nenhum;
        }
        mx = 0;
        my = 0;
        return botao;
    }
    
    public void renderiza(Graphics2D frame, ESTADO estadoDoJogo) {
        
        if (estadoDoJogo == ESTADO.Menu) {
            frame.drawImage(fundo, null, 0, 0);
            frame.drawImage(titulo, null, 240, 75);
            frame.drawImage(jogar, null, 383, 325);
            frame.drawImage(sair, null, 383, 425);
        } else if (estadoDoJogo == ESTADO.Visual) {
            frame.drawImage(fundo, null, 0, 0);
            frame.drawImage(titulo, null, 240, 75);
            frame.drawImage(visual, null, 290, 250);
            frame.drawImage(original, null, 383, 325);
            frame.drawImage(computador, null, 383, 400);
            frame.drawImage(naruto, null, 425, 470);
            frame.drawImage(voltar, null, 383, 575);
        } else if (estadoDoJogo == ESTADO.GameOver) {
            frame.setColor(Color.black);
            frame.fillRect(0, 0, 1280, 720);
            frame.setFont(fabricaDeGraficos.criaFonte(80f));
            frame.setColor(Color.white);
            frame.drawString("GAME OVER", 450, 260);
            frame.setFont(fabricaDeGraficos.criaFonte(60f));
            frame.setColor(Color.white);
            frame.drawString("PONTUACAO  " + JogoController.getJogoModel().getPontuacao(), 430, 360);
            frame.drawString("FASE " + JogoController.getJogoModel().getFase().getNumero(), 550, 400);
        } else if (estadoDoJogo == ESTADO.Victory) {
            frame.setColor(Color.black);
            frame.fillRect(0, 0, 1280, 720);
            frame.setFont(fabricaDeGraficos.criaFonte(80f));
            frame.setColor(Color.white);
            frame.drawString("YOU WIN", 490, 260);
            frame.setFont(fabricaDeGraficos.criaFonte(60f));
            frame.setColor(Color.white);
            frame.drawString("PONTUACAO  " + JogoController.getJogoModel().getPontuacao(), 430, 360);
        }
    }
}