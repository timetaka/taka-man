package controller;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import decorators.comportamentos.Comportamento;
import decorators.comportamentos.ComportamentoBase;
import model.FantasmaModel;
import model.PacManModel;
import util.Direcoes;
import util.GerenciadorPoderes;
import util.Som;
import util.Som.EFEITO;
import view.GraficoView;

public class PacManController extends PersonagemController {

	private boolean vivo = true;
	
	private Comportamento comportamento;
	private ArrayList<GerenciadorPoderes.PODERES> poderes;
	
	public PacManController(int posX, int posY, ArrayList<BufferedImage> arrayList) {
		this.model = new PacManModel(posX, posY);
		this.view = new GraficoView(arrayList);
		this.vivo = true;
		this.comportamento = new ComportamentoBase();
		this.poderes = new ArrayList<>();
	}

	@Override
	public void setMovimento(Direcoes direcao) {
		PacManModel castedModel;
		if (this.model instanceof PacManModel) {
			castedModel = (PacManModel) this.model;
		} else {
			return;
		}

		castedModel.setProximaDirecao(direcao);
		if (!castedModel.getTemParede(direcao)) {
			super.setMovimento(direcao);
		}
	}

	@Override
	public boolean mover() {
		PacManModel castedModel;
		if (this.model instanceof PacManModel) {
			castedModel = (PacManModel) this.model;
		} else {
			return false;
		}

		if (!castedModel.getTemParede(castedModel.getProximaDirecao())) {
			this.setMovimento(castedModel.getProximaDirecao());
		}

		return super.mover();
	}

	@Override
	public boolean colidir(ArrayList<GraficoController> objetosColididos) {
		ArrayList<GraficoController> pacMan = new ArrayList<GraficoController>();
		pacMan.add(this);
		
		boolean transpor = true;
		
		// a colisao do pacman testa se ele pode transpor os outros objetos
		// por isso, para cada colisao que tenha acontecido ao mesmo tempo, eh testado se o objeto eh transponivel ou nao
		for (GraficoController objeto : objetosColididos) {
			if(!(objeto instanceof PacManController)) {
				if (objeto instanceof FantasmaController) {
					if (objeto.getModel() instanceof FantasmaModel) {
						if (((FantasmaModel) objeto.getModel()).isComivel()) {
							transpor = false;
							((FantasmaController) objeto).voltarParaCasinha();
							JogoController.getJogoModel().setPontuacao(JogoController.getJogoModel().getPontuacao() + 500);
							Som.tocarEfeito(EFEITO.COMEFANTASMA);
						} else {
							vivo = false;
							transpor = false;
							Som.tocarEfeito(EFEITO.MORTE);
						}
					}
				}
				transpor = transpor && objeto.colidir(pacMan);
			}
			
		}
		
		return transpor;
	}

	public boolean getVivo() {
		return vivo;
	}

	public void setVivo(boolean vivo) {
		this.vivo = vivo;
	}
	
	public Comportamento getComportamento() {
		return comportamento;
	}	
	
	public void adicionarPoder(GerenciadorPoderes.PODERES poder) {
		GerenciadorPoderes.adicionarPoder(poder, poderes);
		gerarComportamento();
		comportamento.iniciar(this);
	}
	
	private void gerarComportamento() {
		this.comportamento = GerenciadorPoderes.gerarComportamento(this.poderes);
	}
	
}
