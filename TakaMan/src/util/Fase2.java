package util;

import controller.GraficoController;
import view.GraficoFactory;

public class Fase2 implements Fase {

    public void tocarMusica() {
        Som.tocarMusica(2);
    }

	public GraficoController[][] criaMapa(GraficoFactory fabricaDeGraficos) {
        GraficoController[][] mapa = LeitorDeMapa.criaMapa("fase2.txt", fabricaDeGraficos);
        return mapa;
    }
    
    public int getNumero() {
        return 2;
    }

}