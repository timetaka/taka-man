package util;

import controller.GraficoController;
import view.GraficoFactory;

public class Fase1 implements Fase {

    public void tocarMusica() {
        Som.tocarMusica(1);
    }

	public GraficoController[][] criaMapa(GraficoFactory fabricaDeGraficos) {
        GraficoController[][] mapa = LeitorDeMapa.criaMapa("fase1.txt", fabricaDeGraficos);
        return mapa;
    }
    
    public int getNumero() {
        return 1;
    }
    
}