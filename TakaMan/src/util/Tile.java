package util;

public abstract class Tile {
	private static int tamanhoEmPixels = 16;
	
	public static int pixelParaTile(int pixel) {
		return (pixel / tamanhoEmPixels) + 1;
	}
	
	public static int centroTileParaPixel(int tile) {
		return (tile * tamanhoEmPixels) - (tamanhoEmPixels / 2);
	}
	
}
