package util;

import java.util.HashMap;

import controller.GraficoController;
import view.GraficoFactory;

public interface Fase {
    
    public static final HashMap<Class, Fase> proximaFase = new HashMap<Class, Fase>(){{
        put(Fase2.class, new Fase1());
        put(Fase1.class, new Fase2());
    }};
    
    public void tocarMusica();
    
    public GraficoController[][] criaMapa(GraficoFactory fabricaDeGraficos);
    
    public int getNumero();

}