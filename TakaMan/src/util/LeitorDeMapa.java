package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import controller.GraficoController;
import controller.JogoController;
import controller.PacManController;
import controller.EntradaSpawnController;
import controller.FantasmaController;
import controller.ParedeController;
import controller.ParedeFantasmaController;
import controller.PontoController;
import util.GerenciadorPoderes.PODERES;
import view.GraficoFactory;

public abstract class LeitorDeMapa {

	private static final int LINE_FEED_CHARACTER = 10;
	private static final int CARRIAGE_RETURN_CHARACTER = 13;
	private static final int EOF = -1;
	
	
	public static GraficoController[][] criaMapa(String nomeArquivo, GraficoFactory fabricaDeGraficos) {
		char[][] mapaCaracteres = lerArquivo(nomeArquivo);
		GraficoController[][] mapaObjetos = new GraficoController[21][27];

		for (int i = 0; i < mapaCaracteres.length; i++) {
			for (int j = 0; j < mapaCaracteres[0].length; j++) {
				mapaObjetos[j][i] = criaObjeto(mapaCaracteres[i][j], 488 + 16*j, 152 + 16*i, fabricaDeGraficos);
			}
		}
		
		return mapaObjetos;
	}

	private static char[][] lerArquivo(String nomeArquivo) {
		FileReader leitorArquivo;
		char[][] mapa = new char[27][21];
		char caracterLido;
		int valorLido = 0;
		
		try {
			leitorArquivo = new FileReader("mapas/" + nomeArquivo);
			BufferedReader leitor = new BufferedReader(leitorArquivo);
			
			valorLido = leitor.read();
			
			if(valorLido == EOF) {
				System.out.println("Arquivo vazio");
			}
			
			
			int linha = 0, coluna = 0;
			
			do {
				if(valorLido != LINE_FEED_CHARACTER && valorLido != CARRIAGE_RETURN_CHARACTER) {
					caracterLido = (char) valorLido;
					mapa[linha][coluna] = caracterLido;
					coluna++;
				}
				
				if(coluna > 20) {
					coluna = 0;
					linha++;
				}
				
				valorLido = leitor.read();

			}while(valorLido != EOF);
			
			leitor.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("Arquivo nao encontrado");
			return null;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return mapa;
		
	}

	private static GraficoController criaObjeto(char simbolo, int posX, int posY, GraficoFactory fabricaDeGraficos) {

		switch (simbolo) {
		case 'J':
			return new PacManController(posX, posY, fabricaDeGraficos.criaImagem("PacMan"));
		case '-':
			return new ParedeController(posX, posY, fabricaDeGraficos.criaImagem("Parede"));
		case '.':
			JogoController.getJogoModel().setPontos(JogoController.getJogoModel().getPontos() + 1);
			return new PontoController(posX, posY, false, fabricaDeGraficos.criaImagem("Ponto"), PODERES.INERTE);
		case 'B':
			return new FantasmaController(posX, posY, false, fabricaDeGraficos.criaImagem("Fantasma1"), Fantasmas.BLINKY);
		case 'P':
			return new FantasmaController(posX, posY, false, fabricaDeGraficos.criaImagem("Fantasma4"), Fantasmas.PINKY);
		case 'I':
			return new FantasmaController(posX, posY, false, fabricaDeGraficos.criaImagem("Fantasma3"), Fantasmas.INKY);
		case 'C':
			return new FantasmaController(posX, posY, false, fabricaDeGraficos.criaImagem("Fantasma2"), Fantasmas.CLYDE);
		case 'W':
			return new ParedeFantasmaController(posX, posY, null);
		case 'E':
			return new EntradaSpawnController(posX, posY, null);
		case '1':
			JogoController.getJogoModel().setPontos(JogoController.getJogoModel().getPontos() + 1);
			return new PontoController(posX, posY, false, fabricaDeGraficos.criaImagem("EspecialA"), PODERES.CORAGEM);
		case '2':
			JogoController.getJogoModel().setPontos(JogoController.getJogoModel().getPontos() + 1);
			return new PontoController(posX, posY, false, fabricaDeGraficos.criaImagem("EspecialB"), PODERES.RAPIDEZ);
		}

		return new PontoController(posX, posY, true, fabricaDeGraficos.criaImagem("Ponto"), PODERES.INERTE);
	}

}
