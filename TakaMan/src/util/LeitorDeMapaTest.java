package util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import controller.GraficoController;
import controller.FantasmaController;
import controller.PacManController;
import controller.ParedeController;
import controller.PontoController;
import view.GraficoFactory;
import view.OriginalFactory;

class LeitorDeMapaTest {
	
	
	@Test
	void test1() {
		GraficoFactory fabricaDeGraficos = new OriginalFactory();
		GraficoController[][] graficoController = LeitorDeMapa.criaMapa("fase1.txt", fabricaDeGraficos);
		assertEquals(true, graficoController[0][0] instanceof ParedeController);
	}
	
	@Test
	void test2() {
		GraficoFactory fabricaDeGraficos = new OriginalFactory();
		GraficoController[][] graficoController = LeitorDeMapa.criaMapa("fase1.txt", fabricaDeGraficos);
		assertEquals(true, graficoController[5][14] instanceof PontoController);
	}

	@Test
	void test3() {
		GraficoFactory fabricaDeGraficos = new OriginalFactory();
		GraficoController[][] graficoController = LeitorDeMapa.criaMapa("fase1.txt", fabricaDeGraficos);
		assertEquals(true, graficoController[9][12] instanceof FantasmaController);
	}
	
	@Test
	void test4() {
		GraficoFactory fabricaDeGraficos = new OriginalFactory();
		GraficoController[][] graficoController = LeitorDeMapa.criaMapa("fase1.txt", fabricaDeGraficos);
		assertEquals(true, graficoController[10][20] instanceof PacManController);
	}
	
	@Test
	void test5() {
		GraficoFactory fabricaDeGraficos = new OriginalFactory();
		GraficoController[][] graficoController = LeitorDeMapa.criaMapa("fase1.txt", fabricaDeGraficos);
		assertEquals(true, graficoController[19][25] instanceof PontoController);
	}

	@Test
	void test6() {
		GraficoFactory fabricaDeGraficos = new OriginalFactory();
		GraficoController[][] graficoController = LeitorDeMapa.criaMapa("fase1.txt", fabricaDeGraficos);
		assertEquals(true, graficoController[20][10] instanceof PontoController);
	}
}
