package util;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import controller.PersonagemController;

public class MovimentoComSetas implements KeyListener {
	private PersonagemController controladorDoPersonagem;
	
	public MovimentoComSetas(PersonagemController controladorDoPersonagem) {
		this.controladorDoPersonagem = controladorDoPersonagem;
	}

	@Override
	public void keyPressed(KeyEvent evento) {
		int tecla = evento.getKeyCode();
		switch (tecla) {
		case KeyEvent.VK_RIGHT:
			controladorDoPersonagem.setMovimento(Direcoes.DIREITA);
			break;
		case KeyEvent.VK_LEFT:
			controladorDoPersonagem.setMovimento(Direcoes.ESQUERDA);
			break;
		case KeyEvent.VK_UP:
			controladorDoPersonagem.setMovimento(Direcoes.CIMA);
			break;
		case KeyEvent.VK_DOWN:
			controladorDoPersonagem.setMovimento(Direcoes.BAIXO);
			break;
		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent evento) {
	}

	@Override
	public void keyTyped(KeyEvent evento) {
	}

}
