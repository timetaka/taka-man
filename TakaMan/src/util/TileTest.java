package util;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TileTest {

	@Test
	void test() {
		assertEquals(2, Tile.pixelParaTile(31));
	}

	@Test
	void test2() {
		assertEquals(15, Tile.pixelParaTile(236));
	}
	
	@Test
	void test3() {
		assertEquals(118, Tile.pixelParaTile(1887));
	}
	
	@Test
	void test4() {
		assertEquals(6, Tile.pixelParaTile(93));
	}
	
	@Test
	void test5() {
		assertEquals(11813, Tile.pixelParaTile(188996));
	}
	
	@Test
	void test6() {
		assertEquals(24, Tile.centroTileParaPixel(2));
	}
	
	@Test
	void test7() {
		assertEquals(8216, Tile.centroTileParaPixel(514));
	}
	
	@Test
	void test8() {
		assertEquals(1864, Tile.centroTileParaPixel(117));
	}
	
	@Test
	void test9() {
		assertEquals(200856, Tile.centroTileParaPixel(12554));
	}
	
	@Test
	void test10() {
		assertEquals(152, Tile.centroTileParaPixel(10));
	}
}
