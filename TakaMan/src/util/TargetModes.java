package util;

public enum TargetModes {
	SELF,
	LEAVEHOUSE,
	SCATTER,
	CHASE,
	FRIGHTENED
}
