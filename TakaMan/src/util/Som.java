package util;

import java.io.File;
import java.util.HashMap;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Som {

    private static Clip clipMusica = null;
    private static Clip clipEfeitos = null;
    private static int faixaAtual = -1;
    private static EFEITO efeitoAtual = null;

    public enum EFEITO {
        WAKA,
        MORTE,
        COMEFANTASMA
    }

    private static final HashMap<EFEITO, String> mapaDeEfeitos = new HashMap<EFEITO, String>(){{
        put(EFEITO.WAKA, "res/musicas/waka.wav");
        put(EFEITO.MORTE, "res/musicas/morte.wav");
        put(EFEITO.COMEFANTASMA, "res/musicas/comefantasma.wav");
    }};

    private static final HashMap<Integer, String> mapaDeMusicas = new HashMap<Integer, String>(){{
        put(-1, "res/musicas/gameover.wav");
        put(0, "res/musicas/menu.wav");
        put(1, "res/musicas/haruka.wav");
        put(2, "res/musicas/luta.wav");
    }};

    public static void tocarMusica(int faixa) {
        if (faixa == faixaAtual) {
            return;
        }

        if (clipMusica != null) {
            clipMusica.stop();
        }

        try {
            faixaAtual = faixa;
            File music = new File(mapaDeMusicas.get(faixa));
            AudioInputStream audioInput = AudioSystem.getAudioInputStream(music);
            clipMusica = AudioSystem.getClip();
            clipMusica.open(audioInput);
            clipMusica.start();
            clipMusica.loop(Clip.LOOP_CONTINUOUSLY);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void tocarEfeito(EFEITO efeito) {
        if (clipEfeitos != null) {
            if (efeito == efeitoAtual && clipEfeitos.isActive()) {
                return;
            }
        }

        try {
            efeitoAtual = efeito;
            File music = new File(mapaDeEfeitos.get(efeito));
            AudioInputStream audioInput = AudioSystem.getAudioInputStream(music);
            clipEfeitos = AudioSystem.getClip();
            clipEfeitos.open(audioInput);
            clipEfeitos.start();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}