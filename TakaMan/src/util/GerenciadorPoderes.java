package util;

import java.util.ArrayList;
import java.util.Iterator;

import decorators.comportamentos.Comportamento;
import decorators.comportamentos.ComportamentoBase;
import decorators.comportamentos.CoragemDecorator;
import decorators.comportamentos.RapidezDecorator;

public abstract class GerenciadorPoderes {
	public enum PODERES {
		INERTE,
		CORAGEM,
		RAPIDEZ
	}
	
	public static void adicionarPoder(PODERES poder, ArrayList<PODERES> poderes) {
		if (poder == PODERES.INERTE) {
			poderes.clear();
		} else {
			Iterator<GerenciadorPoderes.PODERES> poderesIterator = poderes.iterator();
			boolean poderNaLista = false;
			
			while(poderesIterator.hasNext()) {
				if (poderesIterator.next() == poder) {
					poderNaLista = true;
				}
			}
			
			if (!poderNaLista) {
				poderes.add(poder);
			}
		}
	}
	
    public static Comportamento gerarComportamento(ArrayList<PODERES> poderes) {
    	Comportamento compBase = new ComportamentoBase();
    	
    	if (poderes.size() == 0) {
    		return compBase;
    	} else {
    		Iterator<GerenciadorPoderes.PODERES> poderesIterator = poderes.iterator();
    		Comportamento novoComp = compBase;
    		
    		while (poderesIterator.hasNext()) {
    			PODERES poderAtual = poderesIterator.next();
    			novoComp = montarComportamento(poderAtual, novoComp);
    		}
    		
    		return novoComp;
    	}
    }

    private static Comportamento montarComportamento(PODERES poder, Comportamento compBase) {
    	switch(poder) {
		case CORAGEM:
			return new CoragemDecorator(compBase);
		case INERTE:
			return compBase;
		case RAPIDEZ:
			return new RapidezDecorator(compBase);
		default:
			return compBase;
    	}
    }
}
