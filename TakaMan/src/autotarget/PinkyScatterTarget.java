package autotarget;

import model.FantasmaModel;

public class PinkyScatterTarget extends ScatterTarget {
	private FantasmaModel fantasmaModel;
	
	public PinkyScatterTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}

	@Override
	public void updateTarget() {
		fantasmaModel.setAlvoTileX(34);
		fantasmaModel.setAlvoTileY(9);
	}

}
