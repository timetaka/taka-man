package autotarget;

import controller.JogoController;
import model.FantasmaModel;

public class FrightenedTarget extends AutoTarget {
	FantasmaModel fantasmaModel;
	
	public FrightenedTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}
	
	@Override
	public void updateTarget() {
		fantasmaModel.setAlvoTileX((-1) * JogoController.getPacManController().getModel().getTileX());
		fantasmaModel.setAlvoTileY((-1) * JogoController.getPacManController().getModel().getTileY());
		
	}
	
}
