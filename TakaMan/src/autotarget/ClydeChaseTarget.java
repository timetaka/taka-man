package autotarget;

import controller.JogoController;
import controller.PacManController;
import model.FantasmaModel;
import model.PacManModel;

public class ClydeChaseTarget extends ChaseTarget {
	FantasmaModel fantasmaModel;
	
	public ClydeChaseTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}

	@Override
	public void updateTarget() {
		if (JogoController.getPacManController() instanceof PacManController) {
			PacManModel pacManModel = (PacManModel) JogoController.getPacManController().getModel();
			
			int deltaTileX = fantasmaModel.getTileX() - pacManModel.getTileX();
			int deltaTileY = fantasmaModel.getTileY() - pacManModel.getTileY();
			
			if (deltaTileX < 0) {
				deltaTileX *= (-1);
			}
			
			if (deltaTileY < 0) {
				deltaTileY *= (-1);
			}
			
			if (deltaTileX + deltaTileY > 8) {
				this.fantasmaModel.setAlvoTileX(JogoController.getPacManController().getModel().getTileX());
				this.fantasmaModel.setAlvoTileY(JogoController.getPacManController().getModel().getTileY());
			} else {
				fantasmaModel.setAlvoTileX(34);
				fantasmaModel.setAlvoTileY(39);
			}
			
			
		}
	}

}
