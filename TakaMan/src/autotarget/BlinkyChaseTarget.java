package autotarget;

import controller.JogoController;
import controller.PacManController;
import model.FantasmaModel;

public class BlinkyChaseTarget extends ChaseTarget {
	FantasmaModel fantasmaModel;
	
	public BlinkyChaseTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}

	@Override
	public void updateTarget() {
		if (JogoController.getPacManController() instanceof PacManController) {
			this.fantasmaModel.setAlvoTileX(JogoController.getPacManController().getModel().getTileX());
			this.fantasmaModel.setAlvoTileY(JogoController.getPacManController().getModel().getTileY());
		}
	}

}
