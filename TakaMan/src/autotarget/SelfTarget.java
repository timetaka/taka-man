package autotarget;

import model.FantasmaModel;
import util.Tile;

public class SelfTarget extends AutoTarget {
	FantasmaModel fantasmaModel;
	
	public SelfTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}

	@Override
	public void updateTarget() {
		fantasmaModel.setAlvoTileX(Tile.pixelParaTile(fantasmaModel.getPosicaoInicialX()));
		fantasmaModel.setAlvoTileY(Tile.pixelParaTile(fantasmaModel.getPosicaoInicialY()));
	}

}
