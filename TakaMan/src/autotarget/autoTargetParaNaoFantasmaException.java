package autotarget;

public class autoTargetParaNaoFantasmaException extends Exception {
	private static final long serialVersionUID = 1L;

	public void showMessage() {
		System.out.println("ERRO: Um algoritmo de IA foi associado a um objeto nao-fantasma!");
	}
}
