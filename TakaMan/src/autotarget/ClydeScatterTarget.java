package autotarget;

import model.FantasmaModel;

public class ClydeScatterTarget extends ScatterTarget {
	private FantasmaModel fantasmaModel;
	
	public ClydeScatterTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}
	
	@Override
	public void updateTarget() {
		fantasmaModel.setAlvoTileX(34);
		fantasmaModel.setAlvoTileY(39);
	}

}
