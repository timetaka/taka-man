package autotarget;

import model.FantasmaModel;

public class LeaveHouseTarget extends AutoTarget {
	private FantasmaModel fantasmaModel;
	
	public LeaveHouseTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}

	@Override
	public void updateTarget() {
		fantasmaModel.setAlvoTileX(41);
		fantasmaModel.setAlvoTileY(21);
	}
	
}
