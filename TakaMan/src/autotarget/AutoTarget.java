package autotarget;

import model.FantasmaModel;
import util.TargetModes;

public abstract class AutoTarget {

	public abstract void updateTarget();
	
	public static AutoTarget getAutoTarget(FantasmaModel fantasmaModel, TargetModes targetMode) {
		switch (targetMode) {
		case CHASE:
			return ChaseTarget.getChaseTarget(fantasmaModel);
		case LEAVEHOUSE:
			return new LeaveHouseTarget(fantasmaModel);
		case SCATTER:
			return ScatterTarget.getScatterTarget(fantasmaModel);
		case SELF:
			return new SelfTarget(fantasmaModel);
		case FRIGHTENED:
			return new FrightenedTarget(fantasmaModel);
		}
		
		return new SelfTarget(fantasmaModel);
	}
}
