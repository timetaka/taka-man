package autotarget;

import controller.JogoController;
import controller.PacManController;
import model.FantasmaModel;
import model.PacManModel;

public class PinkyChaseTarget extends ChaseTarget {
	private FantasmaModel fantasmaModel;
	
	public PinkyChaseTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}
	
	@Override
	public void updateTarget() {
		if (JogoController.getPacManController() instanceof PacManController) {
			PacManModel pacManModel = (PacManModel) JogoController.getPacManController().getModel();
			switch (pacManModel.getDirecao()) {
			case BAIXO:
				fantasmaModel.setAlvoTileX(pacManModel.getTileX());
				fantasmaModel.setAlvoTileY(pacManModel.getTileY() + 4);
				break;
			case CIMA:
				fantasmaModel.setAlvoTileX(pacManModel.getTileX());
				fantasmaModel.setAlvoTileY(pacManModel.getTileY() - 4);
				break;
			case DIREITA:
				fantasmaModel.setAlvoTileX(pacManModel.getTileX() + 4);
				fantasmaModel.setAlvoTileY(pacManModel.getTileY());
				break;
			case ESQUERDA:
				fantasmaModel.setAlvoTileX(pacManModel.getTileX() - 4);
				fantasmaModel.setAlvoTileY(pacManModel.getTileY());
				break;
			default:
				break;
			}
		}
	}

}
