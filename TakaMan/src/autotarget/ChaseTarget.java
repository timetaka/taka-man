package autotarget;

import model.BlinkyModel;
import model.FantasmaModel;
import model.InkyModel;
import model.PinkyModel;

public abstract class ChaseTarget extends AutoTarget {
	
	public abstract void updateTarget();
	
	public static ChaseTarget getChaseTarget(FantasmaModel fantasmaModel) {
		if (fantasmaModel instanceof BlinkyModel) {
			return new BlinkyChaseTarget(fantasmaModel);
		} else if (fantasmaModel instanceof PinkyModel) {
			return new PinkyChaseTarget(fantasmaModel);
		} else if (fantasmaModel instanceof InkyModel) {
			return new InkyChaseTarget(fantasmaModel);
		} else {
			return new ClydeChaseTarget(fantasmaModel);
		}
	}
	
}
