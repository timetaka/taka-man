package autotarget;

import model.FantasmaModel;

public class InkyScatterTarget extends ScatterTarget {
	private FantasmaModel fantasmaModel;
	
	public InkyScatterTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}
	
	@Override
	public void updateTarget() {
		fantasmaModel.setAlvoTileX(48);
		fantasmaModel.setAlvoTileY(39);
	}

}
