package autotarget;

import model.FantasmaModel;

public class BlinkyScatterTarget extends ScatterTarget {
	private FantasmaModel fantasmaModel;
	
	public BlinkyScatterTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}
	
	@Override
	public void updateTarget() {
		fantasmaModel.setAlvoTileX(48);
		fantasmaModel.setAlvoTileY(9);
	}

}
