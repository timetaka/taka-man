package autotarget;

import model.BlinkyModel;
import model.FantasmaModel;
import model.InkyModel;
import model.PinkyModel;

public abstract class ScatterTarget extends AutoTarget {

	public abstract void updateTarget();

	public static ScatterTarget getScatterTarget(FantasmaModel fantasmaModel) {
		if (fantasmaModel instanceof BlinkyModel) {
			return new BlinkyScatterTarget(fantasmaModel);
		} else if (fantasmaModel instanceof PinkyModel) {
			return new PinkyScatterTarget(fantasmaModel);
		} else if (fantasmaModel instanceof InkyModel) {
			return new InkyScatterTarget(fantasmaModel);
		} else {
			return new ClydeScatterTarget(fantasmaModel);
		}
	}
}
