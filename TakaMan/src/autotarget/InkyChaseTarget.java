package autotarget;

import controller.FantasmaController;
import controller.JogoController;
import controller.PacManController;
import model.FantasmaModel;
import model.PacManModel;
import util.Tile;

public class InkyChaseTarget extends ChaseTarget {
	private FantasmaModel fantasmaModel;
	
	public InkyChaseTarget(FantasmaModel fantasmaModel) {
		this.fantasmaModel = fantasmaModel;
	}
	
	@Override
	public void updateTarget() {
		if (JogoController.getPacManController() instanceof PacManController &&
				JogoController.getBlinkyController() instanceof FantasmaController) {
			PacManModel pacManModel = (PacManModel) JogoController.getPacManController().getModel();
			FantasmaModel blinkyFantasmaModel = (FantasmaModel) JogoController.getBlinkyController().getModel();
			
			int auxTargetPosicaoX = 0;
			int auxTargetPosicaoY = 0;
			switch (pacManModel.getDirecao()) {
			case BAIXO:
				auxTargetPosicaoX = Tile.centroTileParaPixel(pacManModel.getTileX());
				auxTargetPosicaoY = Tile.centroTileParaPixel(pacManModel.getTileY() + 2);
				break;
			case CIMA:
				auxTargetPosicaoX = Tile.centroTileParaPixel(pacManModel.getTileX());
				auxTargetPosicaoY = Tile.centroTileParaPixel(pacManModel.getTileY() - 2);
				break;
			case DIREITA:
				auxTargetPosicaoX = Tile.centroTileParaPixel(pacManModel.getTileX() + 2);
				auxTargetPosicaoY = Tile.centroTileParaPixel(pacManModel.getTileY());
				break;
			case ESQUERDA:
				auxTargetPosicaoX = Tile.centroTileParaPixel(pacManModel.getTileX() - 2);
				auxTargetPosicaoY = Tile.centroTileParaPixel(pacManModel.getTileY());
				break;
			default:
				break;
			}
			
			int targetPosicaoX = 2 * (auxTargetPosicaoX - blinkyFantasmaModel.getPosicaoX()) + blinkyFantasmaModel.getPosicaoX();
			int targetPosicaoY = 2 * (auxTargetPosicaoY - blinkyFantasmaModel.getPosicaoY()) + blinkyFantasmaModel.getPosicaoY();
			
			fantasmaModel.setAlvoTileX(Tile.pixelParaTile(targetPosicaoX));
			fantasmaModel.setAlvoTileY(Tile.pixelParaTile(targetPosicaoY));
		} 

	}

}
