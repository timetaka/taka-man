package model;

import util.Fase;
import util.Fase2;

public class JogoModel {

	private int vidas;
	private int pontuacao;
	private int pontos = 0;
	private Fase fase;
	private final Class faseFinal = Fase2.class;
	private GraficoModel[][] mapa;
	
	public JogoModel(int vidas, int pontuacao, Fase fase, GraficoModel[][] mapa) {
		this.vidas = vidas;
		this.pontuacao = pontuacao;
		this.fase = fase;
		this.mapa = mapa;
	}

	public int getVidas() {
		return vidas;
	}

	public void setVidas(int vidas) {
		this.vidas = vidas;
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public int getPontos() {
		return pontos;
	}

	public void setPontos(int pontos) {
		this.pontos = pontos;
	}

	public Fase getFase() {
		return fase;
	}
	
	public void setFase(Fase fase) {
		this.fase = fase;
	}

	public Class getFaseFinal() {
		return faseFinal;
	}

	public GraficoModel[][] getMapa() {
		return mapa;
	}

	public void setMapa(GraficoModel[][] mapa) {
		this.mapa = mapa;
	}

}
