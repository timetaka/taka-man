package model;

public class MapaModel {

	private GraficoModel[][] mapa;

	public GraficoModel[][] getMapa() {
		return mapa;
	}

	public void setMapa(GraficoModel[][] mapa) {
		this.mapa = mapa;
	}

}
