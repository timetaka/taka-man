package model;

import util.GerenciadorPoderes;

public class PontoModel extends GraficoModel {

	private boolean comido;
	private GerenciadorPoderes.PODERES poder;
	private int pontuacao;

	/* Construtor */
	public PontoModel(int posicaoX, int posicaoY, boolean comido, GerenciadorPoderes.PODERES poder) {
		super(posicaoX, posicaoY);
		this.comido = comido;
		this.setPoder(poder);
	}
	
	/* Getters e Setters */
	public boolean isComido() {
		return comido;
	}

	public void setComido(boolean comido) {
		this.comido = comido;
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public GerenciadorPoderes.PODERES getPoder() {
		return poder;
	}

	public void setPoder(GerenciadorPoderes.PODERES poder) {
		this.poder = poder;
	}

}
