package model;

import util.Direcoes;

public class PacManModel extends PersonagemModel {

	private int poder;
	private Direcoes proximaDirecao = Direcoes.CIMA;
	
	public PacManModel(int posicaoX, int posicaoY) {
		super(posicaoX, posicaoY);
	}
	
	public int getPoder() {
		return poder;
	}

	public void setPoder(int poder) {
		this.poder = poder;
	}

	public Direcoes getProximaDirecao() {
		return proximaDirecao;
	}

	public void setProximaDirecao(Direcoes proximaDirecao) {
		this.proximaDirecao = proximaDirecao;
	}

}
