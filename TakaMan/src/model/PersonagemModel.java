package model;

import util.Direcoes;

public abstract class PersonagemModel extends GraficoModel {

	protected int ultimaPosicaoX;
	protected int ultimaPosicaoY;

	protected boolean[] temParede = {false, false, false, false};
	protected boolean[] tinhaParede = {false, false, false, false};

	public PersonagemModel(int posicaoX, int posicaoY) {
		super(posicaoX, posicaoY);
		this.velocidadePadrao = 2; //mantenha a velocidade como potencia de 2 (1, 2, 4, 8, ...)
	}

	@Override
	public void setPosicaoX(int posicaoX) {
		this.ultimaPosicaoX = this.posicaoX;
		super.setPosicaoX(posicaoX);
	}

	@Override
	public void setPosicaoY(int posicaoY) {
		this.ultimaPosicaoY = this.posicaoY;
		super.setPosicaoY(posicaoY);
	}

	public int getUltimaPosicaoX() {
		return ultimaPosicaoX;
	}

	public void setUltimaPosicaoX(int ultimaPosicaoX) {
		this.ultimaPosicaoX = ultimaPosicaoX;
	}

	public int getUltimaPosicaoY() {
		return ultimaPosicaoY;
	}

	public void setUltimaPosicaoY(int ultimaPosicaoY) {
		this.ultimaPosicaoY = ultimaPosicaoY;
	}

	public boolean getTemParede(Direcoes direcao) {
		return temParede[direcao.ordinal()];
	}

	public boolean getTinhaParede(Direcoes direcao) {
		return tinhaParede[direcao.ordinal()];
	}

	public void setTemParede(boolean temParede, Direcoes direcao) {
		this.tinhaParede[direcao.ordinal()] = this.temParede[direcao.ordinal()];
		this.temParede[direcao.ordinal()] = temParede;
	}

}
