package model;

import autotarget.AutoTarget;
import util.TargetModes;

public abstract class FantasmaModel extends PersonagemModel {

	private boolean comivel;
	private boolean moving;
	private int alvoTileX;
	private int alvoTileY;
	private AutoTarget autoTarget;
	private TargetModes targetMode;
	private TargetModes antigoTargetMode;
	private boolean direcaoReversivel;
	
	public FantasmaModel(int posicaoX, int posicaoY, boolean comivel) {
		super(posicaoX, posicaoY);
		this.comivel = comivel;
		this.moving = false;
		this.alvoTileX = 48;
		this.alvoTileY = 39;
		this.autoTarget = AutoTarget.getAutoTarget(this, TargetModes.SCATTER);
		this.autoTarget.updateTarget();
		this.targetMode = TargetModes.SCATTER;
		this.antigoTargetMode = TargetModes.SCATTER;
		this.direcaoReversivel = false;
	}

	public boolean isComivel() {
		return comivel;
	}

	public void setComivel(boolean comivel) {
		this.comivel = comivel;
	}
	
	public boolean isMoving() {
		return moving;
	}
	
	public void setMoving(boolean moving) {
		this.moving = moving;
	}

	public int getAlvoTileX() {
		return alvoTileX;
	}

	public void setAlvoTileX(int alvoTileX) {
		this.alvoTileX = alvoTileX;
	}

	public int getAlvoTileY() {
		return alvoTileY;
	}

	public void setAlvoTileY(int alvoTileY) {
		this.alvoTileY = alvoTileY;
	}

	public AutoTarget getAutoTarget() {
		return autoTarget;
	}

	public void setAutoTarget(AutoTarget autoTarget) {
		this.autoTarget = autoTarget;
	}
	
	public TargetModes getTargetMode() {
		return targetMode;
	}

	public void setTargetMode(TargetModes targetMode) {
		if (targetMode != this.targetMode) {
			this.setAntigoTargetMode(this.targetMode);
		}
		this.targetMode = targetMode;
		
		this.setAutoTarget(AutoTarget.getAutoTarget(this, targetMode));
		this.setDirecaoReversivel(true);
	}

	public boolean isDirecaoReversivel() {
		return direcaoReversivel;
	}

	public void setDirecaoReversivel(boolean direcaoReversivel) {
		this.direcaoReversivel = direcaoReversivel;
	}

	public TargetModes getAntigoTargetMode() {
		return antigoTargetMode;
	}

	public void setAntigoTargetMode(TargetModes antigoTargetMode) {
		this.antigoTargetMode = antigoTargetMode;
	}

}
