package model;

import util.Direcoes;
import util.Tile;

public abstract class GraficoModel {

	protected int posicaoX, posicaoY;
	protected int posicaoInicialX, posicaoInicialY;
	protected int tileX, tileY;
	protected int velocidadeX = 0, velocidadeY = 0;
	protected int velocidadePadrao = 0;
	protected Direcoes direcao = Direcoes.ESQUERDA;

	public GraficoModel(int posicaoX, int posicaoY) {
		this.posicaoX = posicaoX;
		this.posicaoY = posicaoY;
		this.posicaoInicialX = posicaoX;
		this.posicaoInicialY = posicaoY;
		this.tileX = Tile.pixelParaTile(posicaoX);
		this.tileY = Tile.pixelParaTile(posicaoY);
	}
	
	public int getPosicaoX() {
		return posicaoX;
	}

	public void setPosicaoX(int posicaoX) {
		this.posicaoX = posicaoX;
		this.tileX = Tile.pixelParaTile(posicaoX);
	}

	public int getPosicaoY() {
		return posicaoY;
	}

	public void setPosicaoY(int posicaoY) {
		this.posicaoY = posicaoY;
		this.tileY = Tile.pixelParaTile(posicaoY);
	}
	
	public int getPosicaoInicialX() {
		return posicaoInicialX;
	}

	public void setPosicaoInicialX(int posicaoInicialX) {
		this.posicaoInicialX = posicaoInicialX;
	}

	public int getPosicaoInicialY() {
		return posicaoInicialY;
	}

	public void setPosicaoInicialY(int posicaoInicialY) {
		this.posicaoInicialY = posicaoInicialY;
	}
	
	public int getTileX() {
		return this.tileX;
	}
	
	public int getTileY() {
		return this.tileY;
	}

	public int getVelocidadeX() {
		return velocidadeX;
	}

	public void setVelocidadeX(int velocidadeX) {
		this.velocidadeX = velocidadeX;
	}

	public int getVelocidadeY() {
		return velocidadeY;
	}

	public void setVelocidadeY(int velocidadeY) {
		this.velocidadeY = velocidadeY;
	}
	
	public int getVelocidadePadrao() {
		return velocidadePadrao;
	}

	public void setVelocidadePadrao(int velocidadePadrao) {
		if (this.posicaoX == Tile.centroTileParaPixel(this.tileX) && this.posicaoY == Tile.centroTileParaPixel(this.tileY)) {
			this.velocidadePadrao = velocidadePadrao;
		}
	}

	public Direcoes getDirecao() {
		return direcao;
	}

	public void setDirecao(Direcoes direcao) {
		this.direcao = direcao;
	}

}
