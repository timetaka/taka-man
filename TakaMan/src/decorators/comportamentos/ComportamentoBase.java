package decorators.comportamentos;

import controller.PacManController;

public class ComportamentoBase implements Comportamento {
	
	public ComportamentoBase() {
		
	}

	@Override
	public void iniciar(PacManController pacman) {
		
	}

	@Override
	public void checarSeTerminou(PacManController pacman) {
	}

}
