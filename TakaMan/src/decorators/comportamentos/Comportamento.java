package decorators.comportamentos;

import controller.PacManController;

public interface Comportamento {
	public void iniciar(PacManController pacman);
	public void checarSeTerminou(PacManController pacman);
}
