package decorators.comportamentos;

import java.util.Iterator;

import controller.FantasmaController;
import controller.JogoController;
import controller.PacManController;
import util.Tile;
import util.GerenciadorPoderes.PODERES;

public abstract class ComportamentoDecorator implements Comportamento {
	public Comportamento comportamento;
	
	public ComportamentoDecorator(Comportamento comportamento) {
		this.comportamento = comportamento;
	}
	
	public void setComportamento(Comportamento comportamento) {
		this.comportamento = comportamento;
	}

	@Override
	public void iniciar(PacManController pacman) {
		
	}
	
	@Override
	public void checarSeTerminou(PacManController pacman) {
		
	}
	
	protected void retornaEstadoInicial(PacManController pacman) {
		if (podeMudarVel(pacman)) {
			alternaFantasmas(false);
			pacman.getModel().setVelocidadePadrao(2);
			pacman.adicionarPoder(PODERES.INERTE);
		}
		
	}

	protected void alternaFantasmas(boolean comMedo) {
		Iterator<FantasmaController> fantasmaIterator = JogoController.getFantasmasControllers();
		
		while(fantasmaIterator.hasNext()) {
			FantasmaController fantasmaController = fantasmaIterator.next();
			fantasmaController.definirComportamentoMedo(comMedo);
		}
	}
	
	protected boolean podeMudarVel(PacManController pacman) {
		return pacman.getModel().getPosicaoX() == Tile.centroTileParaPixel(pacman.getModel().getTileX())
				&& pacman.getModel().getPosicaoY() == Tile.centroTileParaPixel(pacman.getModel().getTileY());
	}

}
