package decorators.comportamentos;

import controller.PacManController;
import util.Tile;

public class RapidezDecorator extends ComportamentoDecorator {

	private long tempo;
	private long tempoAnterior;
	
	public RapidezDecorator(Comportamento comportamento) {
		super(comportamento);
		tempoAnterior = System.currentTimeMillis();
	}

	@Override
	public void iniciar(PacManController pacman) {
		super.iniciar(pacman);
	}
	
	@Override
	public void checarSeTerminou(PacManController pacman) {
		super.checarSeTerminou(pacman);
		tempo = tempo + System.currentTimeMillis() - tempoAnterior;
		tempoAnterior = System.currentTimeMillis();
		
		if (podeMudarVel(pacman)) {
			if(tempo > 6000) {
				retornaEstadoInicial(pacman);
				return;
			} else {
				ficaMaisRapido(pacman);
			}
		}

	}
	
	private void ficaMaisRapido(PacManController pacman) {
		pacman.getModel().setVelocidadePadrao(4);
	}
}
