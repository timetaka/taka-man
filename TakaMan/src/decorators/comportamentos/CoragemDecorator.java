package decorators.comportamentos;

import controller.PacManController;

public class CoragemDecorator extends ComportamentoDecorator {
	
	private long tempo;
	private long tempoAnterior;

	public CoragemDecorator(Comportamento comportamento) {
		super(comportamento);
		tempoAnterior = System.currentTimeMillis();	
	}
	
	@Override
	public void iniciar(PacManController pacman) {
		super.iniciar(pacman);
		alternaFantasmas(true);
	}
	
	@Override
	public void checarSeTerminou(PacManController pacman) {
		super.checarSeTerminou(pacman);
		
		tempo = tempo + System.currentTimeMillis() - tempoAnterior;
		tempoAnterior = System.currentTimeMillis();
		
		if(tempo > 6000) {
			retornaEstadoInicial(pacman);
			return;
		}
	}
}
