package main;

import java.lang.Math;

import controller.JogoController;

public class ExecutaJogo implements Runnable {

	private Thread thread;
	private static final int FPS = 60;
	private boolean executando = false;

	private static JogoController jogo;

	public static void main(String[] args) {
		jogo = new JogoController();
		new ExecutaJogo();
	}

	public ExecutaJogo() {
		this.start();
	}

	@Override
	public void run() {
		while (executando) {
			long start = System.currentTimeMillis();

			jogo.atualiza();

			try {
				long sleep = Math.max(start + 1000/FPS - System.currentTimeMillis(), 0L);
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void start() {
		thread = new Thread(this);
		thread.start();
		executando = true;
	}

	public synchronized void stop() {
		try {
			thread.join();
			executando = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	


}