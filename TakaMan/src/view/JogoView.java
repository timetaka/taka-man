package view;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import javax.swing.JFrame;

import controller.Menu;
import util.MovimentoComSetas;
 
public class JogoView extends Canvas {

    private static final long serialVersionUID = -4421470309876743885L;

    private static final String TITULO = "TakaMan";
	private static final int LARGURA = 1280;
	private static final int ALTURA = 720;

    private JFrame frame;
    BufferStrategy bufferStrategy = null;
    private MapaView mapaView = null;
    private HUDView hud = null;

    public JogoView(Menu menu) {
		frame = new JFrame(TITULO);
		
		frame.setPreferredSize(new Dimension(LARGURA, ALTURA));
		frame.setMaximumSize(new Dimension(3*LARGURA/2, 3*ALTURA/2));
		frame.setMinimumSize(new Dimension(LARGURA, ALTURA));
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.add(this);
        frame.setVisible(true);
        
        this.addMouseListener(menu);
    }

    public Graphics2D criaFrame() {
        Graphics2D frame = (Graphics2D) bufferStrategy.getDrawGraphics();

        return frame;
    }

    public void renderizaMenu(Graphics2D frame) {
        frame.dispose();
        bufferStrategy.show();
    }

    public void renderizaJogo(Graphics2D frame) {
        mapaView.renderizar(frame);
        hud.renderizar(frame);

        frame.dispose();
        bufferStrategy.show();
    }

    public void criaBufferStrategy() {
        this.createBufferStrategy(3);
        bufferStrategy = this.getBufferStrategy();
    }

    public void setGraficos(GraficoFactory fabricaDeGraficos) {
        hud = new HUDView(fabricaDeGraficos);
        mapaView = new MapaView(fabricaDeGraficos);
    }

	public void criaMovimentoComSetas(MovimentoComSetas mcs) {
		this.addKeyListener(mcs);
	}

}
