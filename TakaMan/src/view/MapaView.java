package view;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;

import controller.FantasmaController;
import controller.GraficoController;
import controller.MapaController;

public class MapaView {
    private ArrayList<BufferedImage> fundo = null;

    public MapaView(GraficoFactory fabricaDeGraficos) {
        fundo = fabricaDeGraficos.criaImagem("Fundo");
    }

    public void renderizar(Graphics2D frame) {
        ArrayList<GraficoController> fantasmas = new ArrayList<>();

    	for (BufferedImage sprite: fundo) {
        frame.drawImage(sprite, null, 0, 0);
    	}

        for (GraficoController grafico: MapaController.getMapa()) {
            if (grafico instanceof FantasmaController) {
                fantasmas.add(grafico);
            } else {
                grafico.atualizaView(frame);
            }
        }

        Iterator<GraficoController> fantasmaIterator = fantasmas.iterator();
        while(fantasmaIterator.hasNext()) {
            fantasmaIterator.next().atualizaView(frame);
        }
    }

}
