package view;

import java.util.ArrayList;
import java.util.HashMap;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;

import javax.imageio.ImageIO;

public class NarutoFactory implements GraficoFactory {

    HashMap<String, String> mapaDeImagens = new HashMap<>();

    public NarutoFactory() {
        mapaDeImagens.put("Ponto", "/temaNaruto/ponto");
        mapaDeImagens.put("EspecialA", "/temaNaruto/especial");
        mapaDeImagens.put("EspecialB", "/temaNaruto/especial2");
        mapaDeImagens.put("Parede", "/temaNaruto/parede");
        mapaDeImagens.put("Fantasma1", "/temaNaruto/inimigo2");
        mapaDeImagens.put("Fantasma2", "/temaNaruto/inimigo3");
        mapaDeImagens.put("Fantasma3", "/temaNaruto/inimigo4");
        mapaDeImagens.put("Fantasma4", "/temaNaruto/inimigo6");
        mapaDeImagens.put("Fugitivo", "/temaNaruto/fugitivo");
        mapaDeImagens.put("PacMan", "/temaNaruto/protagonista");
        mapaDeImagens.put("Fundo", "/temaNaruto/fundo");
        mapaDeImagens.put("Fonte", "/temaNaruto/ARCADECLASSIC.ttf");
    }

    @Override
    public ArrayList<BufferedImage> criaImagem(String tipo) {
    	ArrayList<BufferedImage> imagens = new ArrayList<BufferedImage>();
    	
    	try {    		
    		String[] caminhoSprites;
    		File f = new File (System.getProperty("user.dir") + "/res" + mapaDeImagens.get(tipo));
    		caminhoSprites = f.list();
    		
    		for (int i = 0; i < caminhoSprites.length; i++) {
    			BufferedImage sprite = ImageIO.read(getClass().getResource(mapaDeImagens.get(tipo) + "/" + caminhoSprites[i]));
    			imagens.add(sprite);
    		} 		
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	return imagens;
    }

    @Override
    public Font criaFonte(float tamanho) {
        Font font = null;

        try {
            String caminhoFonte = System.getProperty("user.dir") + "/res" + mapaDeImagens.get("Fonte");
            font = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(caminhoFonte)).deriveFont(tamanho);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return font;
    }

}
