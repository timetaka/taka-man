package view;

import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;

import util.Direcoes;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;

public class GraficoView {
    private ArrayList<BufferedImage> imagem = null;
    private static final int larguraImagem = 16;
    private static final int alturaImagem = 16;
    private int numeroImagem = 0;

    private static final Map<Direcoes, AffineTransformOp> mapa = new HashMap<Direcoes, AffineTransformOp>(){{
                        put(Direcoes.DIREITA, null);
                        put(Direcoes.ESQUERDA, espelhar());
                        put(Direcoes.CIMA, rotacionarQuadrante(3));
                        put(Direcoes.BAIXO, rotacionarQuadrante(1));
    }};
    private static final HashMap<Direcoes, AffineTransformOp> mapaDirecaoTransformacao = new HashMap<>(mapa);

    public GraficoView(ArrayList<BufferedImage> bufferedImage) {
        this.imagem = bufferedImage;
    }

    public void renderizar(Graphics2D g, int x, int y, Direcoes direcao) {
        AffineTransformOp at = mapaDirecaoTransformacao.get(direcao);
        g.drawImage(imagem.get(numeroImagem/8 % imagem.size()), at, x, y);
        numeroImagem++;
    }

    private static AffineTransformOp rotacionarQuadrante(int quadrante) {
        AffineTransform rotaciona = AffineTransform.getQuadrantRotateInstance(quadrante, larguraImagem/2, alturaImagem/2);
        return new AffineTransformOp(rotaciona, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
    }

    private static AffineTransformOp espelhar() {
        AffineTransform scale = AffineTransform.getScaleInstance(-1, 1);
        scale.concatenate(AffineTransform.getTranslateInstance(-larguraImagem, 0));
        return new AffineTransformOp(scale, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
    }
    
}
