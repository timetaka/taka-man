package view;

import java.util.ArrayList;
import java.util.HashMap;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;

import javax.imageio.ImageIO;

public class OriginalFactory implements GraficoFactory {

    HashMap<String, String> mapaDeImagens = new HashMap<>();

    public OriginalFactory() {
        mapaDeImagens.put("Ponto", "/original/ponto");
        mapaDeImagens.put("EspecialA", "/original/especial");
        mapaDeImagens.put("EspecialB", "/original/especial2");
        mapaDeImagens.put("Parede", "/original/parede");
        mapaDeImagens.put("Fantasma1", "/original/inimigo1");
        mapaDeImagens.put("Fantasma2", "/original/inimigo2");
        mapaDeImagens.put("Fantasma3", "/original/inimigo3");
        mapaDeImagens.put("Fantasma4", "/original/inimigo4");
        mapaDeImagens.put("Fugitivo", "/original/fugitivo");
        mapaDeImagens.put("PacMan", "/original/protagonista");
        mapaDeImagens.put("Fundo", "/original/fundo");
        mapaDeImagens.put("Fonte", "/original/ARCADECLASSIC.ttf");
    }

    @Override
    public ArrayList<BufferedImage> criaImagem(String tipo) {
    	ArrayList<BufferedImage> imagens = new ArrayList<BufferedImage>();
    	
    	try {    		
    		String[] caminhoSprites;
    		File f = new File (System.getProperty("user.dir") + "/res" + mapaDeImagens.get(tipo));
    		caminhoSprites = f.list();
    		
    		for (int i = 0; i < caminhoSprites.length; i++) {
    			BufferedImage sprite = ImageIO.read(getClass().getResource(mapaDeImagens.get(tipo) + "/" + caminhoSprites[i]));
    			imagens.add(sprite);
    		} 		
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	return imagens;
    }

    @Override
    public Font criaFonte(float tamanho) {
        Font font = null;

        try {
            String caminhoFonte = System.getProperty("user.dir") + "/res" + mapaDeImagens.get("Fonte");
            font = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(caminhoFonte)).deriveFont(tamanho);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return font;
    }
    
}
