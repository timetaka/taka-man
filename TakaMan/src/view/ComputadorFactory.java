package view;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class ComputadorFactory implements GraficoFactory {
	
    HashMap<String, String> mapaDeImagens = new HashMap<>();

    public ComputadorFactory() {
        mapaDeImagens.put("Ponto", "/computador/ponto");
        mapaDeImagens.put("EspecialA", "/computador/especial");
        mapaDeImagens.put("EspecialB", "/computador/especial2");
        mapaDeImagens.put("Parede", "/computador/parede");
        mapaDeImagens.put("Fantasma1", "/computador/inimigo2");
        mapaDeImagens.put("Fantasma2", "/computador/inimigo3");
        mapaDeImagens.put("Fantasma3", "/computador/inimigo4");
        mapaDeImagens.put("Fantasma4", "/computador/inimigo6");
        mapaDeImagens.put("Fugitivo", "/computador/fugitivo");
        mapaDeImagens.put("PacMan", "/computador/protagonista");
        mapaDeImagens.put("Fundo", "/computador/fundo");
        mapaDeImagens.put("Fonte", "/computador/ARCADECLASSIC.ttf");
    }

    @Override
    public ArrayList<BufferedImage> criaImagem(String tipo) {
    	ArrayList<BufferedImage> imagens = new ArrayList<BufferedImage>();
    	
    	try {    		
    		String[] caminhoSprites;
    		File f = new File (System.getProperty("user.dir") + "/res" + mapaDeImagens.get(tipo));
    		caminhoSprites = f.list();
    		
    		for (int i = 0; i < caminhoSprites.length; i++) {
    			BufferedImage sprite = ImageIO.read(getClass().getResource(mapaDeImagens.get(tipo) + "/" + caminhoSprites[i]));
    			imagens.add(sprite);
    		} 		
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	return imagens;
    }

    @Override
    public Font criaFonte(float tamanho) {
        Font font = null;

        try {
            String caminhoFonte = System.getProperty("user.dir") + "/res" + mapaDeImagens.get("Fonte");
            font = Font.createFont(Font.TRUETYPE_FONT, new FileInputStream(caminhoFonte)).deriveFont(tamanho);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return font;
    }

}
