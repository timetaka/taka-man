package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;

import controller.JogoController;
import util.Direcoes;

public class HUDView {
		
	private double escala = 1.5;

	private Font fonte = null;
    private ArrayList<GraficoView> vidasImagens = new ArrayList<>();
	
	
	public HUDView(GraficoFactory fabricaDeGraficos) {
		
		// PRECISA DE REFATORAÇÃO //
		fonte = fabricaDeGraficos.criaFonte(20f);
		
		for (int i = 0; i < JogoController.getJogoModel().getVidas(); i++)	
			vidasImagens.add(new GraficoView(fabricaDeGraficos.criaImagem("PacMan")));
	}
	
	public void renderizar(Graphics2D g2d) {
		
		// TALVEZ MUDAR A FUNÇÃO SCALE. ELA ESCALA LITERALMENTE TUDO, ATÉ AS COORDENADAS  //
		
		g2d.setFont(fonte);
		g2d.setColor(Color.white);
		g2d.scale(escala, escala);
		g2d.drawString("PONTUACAO  " + JogoController.getJogoModel().getPontuacao(), 15, 26);
		g2d.drawString("FASE " + JogoController.getJogoModel().getFase().getNumero(), 420, 26);
		
		
		for (int i = 1; i <= JogoController.getJogoModel().getVidas(); i++) {
			vidasImagens.get(i-1).renderizar(g2d, 700 + (i*30), 15, Direcoes.ESQUERDA);
		}
		
	}
	
	
}
