package view;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public interface GraficoFactory {
    ArrayList<BufferedImage> criaImagem(String tipo);

    Font criaFonte(float tamanho);	
	
}